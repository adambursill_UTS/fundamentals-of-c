	---------------------------------------------------------------------------------------------------
	
										WELCOME TO THE UTS CLI SYSTEM
	
	---------------------------------------------------------------------------------------------------
	Project Overview:
	---------------------------------------------------------------------------------------------------
	This system allows UTS staff and students to connect together in a collaborative and interactive
	environment.Through this interaction, both sides are able to automate the common processes in
	university. Security and performance are high qualities in this project, so both encryption and 
	compression methods have been implemented.
	
	---------------------------------------------------------------------------------------------------
	Project Features:
	---------------------------------------------------------------------------------------------------
	System - Login, Register, Exit
	
	Staff - Change Account Details, Create Subjects, View Subjects, Create Announcements,
	View and Mark Assignments, View Students in Subjects, Logout
	
	Student - Change Account Details, Upload Assignments, View Grades, Enrol in Classes,
	View Announcements, Clear Announcements, Logout
	
	---------------------------------------------------------------------------------------------------
	Runtime Requisites (IMPORTANT):
	---------------------------------------------------------------------------------------------------
	1. Create a directory called "uploaded" in the same directory as the main application file.
	This allows the program to relocate an assignment into this folder from the main directory.
	
	2. Create a directory called "downloaded" in the same directory as the main application file.
	This will allow the program to relocate the assignments to be marked by a staff member from the
	main directory.

	3. To upload an assignment, create a .txt file in the same directory as the main application file.
	Write any string of characters of any length you choose and save it into this file.This allows the
	program to locate the assignments to be uploaded to a subject by a student.
	
	---------------------------------------------------------------------------------------------------
	Entering DEBUG Mode:
	---------------------------------------------------------------------------------------------------
	To enter DEBUG mode in the application:
	
	1. Open the UTS_CLI.h file.
	
	2. Navigate to line 20 and locate the preprocessor debug command.
	
	3. Uncomment the line and allow it to be a part of the program.
	
	4. Make the application again, and DEBUG mode should be enabled.
	
	---------------------------------------------------------------------------------------------------
	
	The UTS CLI System was designed and developed by UTS 48430 Fundamentals of C Programming 
	Spring 2018 Activity 03 Group 28.
	
	Team Members:
	
	Adam Bursill - 12613358
	Berkan Yuksel - 12891848
	Hakan Day - 97117283
	Nathan Ha - 12912642
	Haoran Huang - 12786253
	
	---------------------------------------------------------------------------------------------------

	




