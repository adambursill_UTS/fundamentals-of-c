/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * This is the main entry point for the UTS CLI system it only contains the 
 * main function
 * 
*******************************************************************************/

/* Included C File Headers */
#include "UTS_CLI.h"
#include "UTS_staff_options.h"
#include "UTS_student_options.h"
#include "UTS_system_options.h"
#include "UTS_utility.h"



/* Main */
int main(int argc, char *argv[]) 
{
    UTSCLISystemType system;
    char userInput[MAX_OPTION_INPUT + EXTRA_SPACES];
    char encryptedPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
    int userSelection;
    FILE* file;

    systemInit(&system);

    load_system_state(&system);

    /* Handles when command line arguments are being passed*/
    if(argc > 1)
    {
        if(strcmp(argv[1], ARG_HELP) == 0)
        {
            printf("-------------Welcome to UTS CLI HELP-------------\n");
            printf("You can proceed to the main menu just by entering ./UTS_CLI\n");
            printf("You can also use the following command line passing:\n");
            printf("Note! For logging in throught the command line you must have already registered.\n");
            printf("./UTS_CLI student [studentID] [password] to log straight into the student menu\n");
            printf("./UTS_CLI staff [staffID] [password] to log straight into the staff menu\n");
            printf("./UTS_CLI compress [fileInName] [fileOutName] to compress a txt file\n");
            printf("./UTS_CLI encrypt [fileName] to encrypt a txt file\n");
            printf("./UTS_CLI decrypt [fileName] to decrypt a txt file\n\n");
        }

        /* Handles command line student loggin*/
        if(strcmp(argv[1], ARG_STUDENT) == 0)
        {
            printf("-------------COMMAND LINE STUDENT LOGGIN-------------\n");
            /* Check ID length*/
            if(strlen(argv[2]) != STUDENT_ID_LEN)
            {
                printf("\nError - ID Wrong length\n\n");
                return EXIT_FAILURE;
            }
            /* Encrypt password to use for comparing to their system password */
            encrypt_password(argv[3], encryptedPassword);

    
            StudentTypePtr student = searchForStudentAndReturn(system.headStudentNode, argv[2]);
            /*Check to see if the student member exists and their password does not match*/
            if(student == NULL || strcmp(student->password, encryptedPassword) != 0)
            {
                printf("\nError - ID or Password Incorrect\n\n");
                return EXIT_FAILURE;
            }
            else
            {
                /*Login and proceed to the student main menu*/
                printf("Login successful\n\n");
                main_student_menu(&system, student);
            }
        }       

        /* Handles command line staff loggin */
        if(strcmp(argv[1], ARG_STAFF) == 0)
        {
            printf("-------------COMMAND LINE STAFF LOGGIN-------------\n");
            /* Check ID length*/
            if(strlen(argv[2]) != STAFF_ID_LEN)
            {
                printf("\nError - ID Wrong length\n\n");
                return EXIT_FAILURE;
            }
            /* Encrypt password to use for comparing to their system password */
            encrypt_password(argv[3], encryptedPassword);

            StaffTypePtr staff = searchForStaffAndReturn(system.headStaffNode, argv[2]);
            /*Check to see if the staff member exists and their password does not match*/
            if(staff == NULL || strcmp(staff->password, encryptedPassword) != 0)
            {
                printf("\nError - ID or password incorrect\n\n");
                return EXIT_FAILURE;
            }
            else
            {
                /*Login and proceed to the staff main menu*/
                printf("Login successful\n\n");
                staff_main_menu(&system, staff);
            }
        }

        /* Handles command line compressing request */
        if(strcmp(argv[1], ARG_COMPRESS) == 0)
        {
            printf("-------------COMMAND LINE COMPRESSING-------------\n");

            /* Check the files does exist*/
            file = fopen(argv[2], "r");
            if (file == NULL)
            {
                printf("Error - File does not exist.\n\n");
                return EXIT_FAILURE;
            }
            /* Check to see if file is empty */
            fseek (file, 0, SEEK_END);
            long size = ftell(file);
            if (0 == size) 
            {
                printf("Error - File is empty!\n\n");
                fclose(file);
                return EXIT_FAILURE;
            }            
            fclose(file); /* Done using the reference */


            compress(argv[2], argv[3]);
            printf("Done!\n\n");
        }

        /* Handles command line decompressing request */
        if(strcmp(argv[1], ARG_DECOMPRESS) == 0)
        {
            printf("-------------COMMAND LINE DECOMPRESSING-------------\n");

            /* Check the files does exist*/
            file = fopen(argv[2], "r");
            if (file == NULL)
            {
                printf("Error - File does not exist.\n\n");
                return EXIT_FAILURE;
            }
            /* Check to see if file is empty */
            fseek (file, 0, SEEK_END);
            long size = ftell(file);
            if (0 == size) 
            {
                printf("Error - File is empty!\n\n");
                fclose(file);
                return EXIT_FAILURE;
            }            
            fclose(file); /* Done using the reference */

            extract(argv[2], argv[3]);
            printf("Done!\n\n");
        }

        /* Handles command line encryption request */
        if(strcmp(argv[1], ARG_ENCRYPT) == 0)
        {
            printf("-------------COMMAND LINE ENCRYPTING-------------\n");

            /* Check the files does exist*/
            file = fopen(argv[2], "r");
            if (file == NULL)
            {
                printf("Error - File does not exist.\n\n");
                return EXIT_FAILURE;
            }
            fclose(file);

            encrypt_file(argv[2], ENCRYPTION_KEY);
            printf("Done!\n\n");
        }

        /* Handles command line Decryption request */
        if(strcmp(argv[1], ARG_DECRYPT) == 0)
        {
            printf("-------------COMMAND LINE DECRYPTING-------------\n");

            /* Check the files does exist*/
            file = fopen(argv[2], "r");
            if (file == NULL)
            {
                printf("Error - File does not exist.\n\n");
                return EXIT_FAILURE;
            }
            fclose(file);

            decrypt_file(argv[2], ENCRYPTION_KEY);
            printf("Done!\n\n");
        }


        return EXIT_SUCCESS;
    }    

    /* Normal program operation/loop*/
    do 
    {
        print_main_menu();

        fgets(userInput, MAX_OPTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input ie not new line*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error - Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection); /* Read string into int value*/

        /* switch statement to handle the user menu inputs*/
        switch(userSelection)
        {
            case 1:
                login_user(&system);
                break;
            case 2:
                register_new_user(&system);
                save_system_state(&system);
                break;
            case 3:
                printf("\n--- Exiting ---\n\n");
                save_system_state(&system);
                systemFree(&system);
                break;
                /* These 3 cases are used in debug mode to see the current stat of the systems linked lists*/
            #ifdef DEBUG
                case 4:
                    printf("\nStaff Linked List\n");
                    printf("------- \n\n");
                    printf("Total Number of Staff: %d\n\n", system.numStaff);
                    printStaffLinkedList(system.headStaffNode);
                break;

                case 5:
                    printf("\nStudent Linked List\n");
                    printf("------- \n\n");
                    printf("Total Number of Students: %d\n\n", system.numStudents);
                    printStudentLinkedList(system.headStudentNode);
                break;

                case 6:
                    printf("\nSubject Linked List \n");
                    printf("------- \n\n");
                    printf("Total Number of Subjects: %d\n\n", system.numSubjects);
                    printSubjectLinkedList(system.headSubjectNode);
                break;

            #endif
            /* If the user enter invalid input the defualt case will be called*/
            default:
                printf("Error - Input a single number 1-3\n\n");
                #ifdef DEBUG
                    printf("DEBUG - switch default error case check\n\n");
                #endif
                break;

        }

    }while(userSelection != 3);

    return EXIT_SUCCESS;
}

