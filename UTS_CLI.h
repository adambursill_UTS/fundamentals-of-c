/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * 
*******************************************************************************/


#ifndef UTS_CLI_H
#define UTS_CLI_H

/* System-wide header files. */
#include <stdio.h> /* printf(), scanf(), fseek(), fopen(), fclose()*/
#include <stdlib.h> /* malloc(), size_t */
#include <string.h> /* strlen(), strcmp(), strcpy() */
#include <math.h> /* pow() */

/* Preprocessor debug*/
/*#define DEBUG*/

/* System-wide constants. */
#define STAFF_ID_LEN 6
#define STUDENT_ID_LEN 8
#define SUBJECT_ID_LEN 5
#define MIN_NAME_LEN 1
#define MAX_NAME_LEN 25
#define MAX_SUBJECT_NAME_LEN 50
#define MAX_OPTION_INPUT 1
#define EXTRA_SPACES 2
#define RETURN_TO_MENU -1
#define PROMPT_LENGTH 512
#define MAX_PASSWORD_LENGTH 8
#define MAX_STRING_LENGTH 1024
#define MAX_ALLOWED_SUBJECTS 4
#define MAX_SELECTION_INPUT 3

/* uploading and downloading folder names*/
#define UPLOAD_DIR "uploaded"
#define DOWNLOAD_DIR "downloaded"

/* Command Line Arguments*/

#define ARG_HELP "help"
#define ARG_STUDENT "student"
#define ARG_STAFF "staff"
#define ARG_COMPRESS "compress"
#define ARG_DECOMPRESS "decompress"
#define ARG_ENCRYPT "encrypt"
#define ARG_DECRYPT "decrypt"


/*Boolean enum definition*/
typedef enum BOOLEAN
{
	FALSE = 0,

	TRUE = 1

}BOOLEAN;

/* Linked list node pointer deffinitions*/
typedef struct staff* StaffTypePtr;
typedef struct student* StudentTypePtr;
typedef struct subject* SubjectTypePtr;
typedef struct string* StringTypePtr;


/* Linked list structure definitions */
/* staff linked list*/
typedef struct staff
{
    char staffID[STAFF_ID_LEN + 1];
    char staffName[MAX_NAME_LEN + 1];
    char password[MAX_PASSWORD_LENGTH + 1];

     /*References to what subjects the teacher is incharge of*/
    StringTypePtr subjectsHeadNode;
    int numOfSubjectsManaged;


   StaffTypePtr nextStaffNode;
} StaffType;

/* Used to store data about assignment, not a linked list*/
typedef struct assignment
{
    char assignmentName[MAX_SUBJECT_NAME_LEN + 1];
    char filename[MAX_SUBJECT_NAME_LEN + 1];
    char subject[SUBJECT_ID_LEN + 1];
    float mark;
} AssignmentType;

/* student linked list*/
typedef struct student
{
    char studentID[STUDENT_ID_LEN + 1];
    char studentName[MAX_NAME_LEN + 1];
    char password[MAX_PASSWORD_LENGTH + 1];

    /* Used to hold references to what subjects the student is enrol in*/
    char enroledClasses[MAX_ALLOWED_SUBJECTS][SUBJECT_ID_LEN + 1];
    int numOfEnrolledClasses;

    /* Internal string linked list to hold annoucnements*/
    StringTypePtr announcementsHeadNode;
    int numOfAnnoucements;

    /* Array of assignments structs*/
    AssignmentType assignments[MAX_ALLOWED_SUBJECTS]; /*Only allowed 1 assignment per subject*/
    int numOfAssignments;

    StudentTypePtr nextStudentNode;
} StudentType;

/* subject linked list*/
typedef struct subject
{
    char subjectID[SUBJECT_ID_LEN + 1];
    char subjectName[MAX_SUBJECT_NAME_LEN + 1];
    /* Internal string linked list to hold refernces to student enroled in subject*/
    StringTypePtr studentIDsEnrolledHead;
    int numOfEnrolledStudents;

    SubjectTypePtr nextSubjectNode;
} SubjectType;

/* string linked list*/
typedef struct string
{
    char string[MAX_STRING_LENGTH + 1];

    StringTypePtr nextStringNode;

} StringType;

/* Main data structure of the system*/
typedef struct uts_cli_system
{
   StaffTypePtr headStaffNode;
   unsigned numStaff;

   StudentTypePtr headStudentNode;
   unsigned numStudents;

   SubjectTypePtr headSubjectNode;
   unsigned numSubjects;

} UTSCLISystemType;

#endif
