/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * This file holds all of the staff menu and the options that that menu can 
 * navigate too
*******************************************************************************/


/* Included C File Headers */
#include "UTS_CLI.h"
#include "UTS_staff_options.h"
#include "UTS_student_options.h"
#include "UTS_system_options.h"
#include "UTS_utility.h"

/*******************************************************************************
 * Description: Prints the initial staff menu and expects input choice
 * inputs: Pointer to the current system, pointer to the current staff
 * outputs: None
 * Author: Hakan Day
*******************************************************************************/
void staff_main_menu(UTSCLISystemType* system, StaffTypePtr staff) 
{
    char userInput[MAX_OPTION_INPUT + EXTRA_SPACES];
    int userSelection;
            
    do
    {
        printf("Welcome to staff main menu %s\n" /* Print menu */
                "1. Change Account Details\n"
                "2. Create New Subject\n"
                "3. View Existing Subjects\n"
                "4. New Announcement\n"
                "5. View and Mark Assignment Submissons\n"
                "6. View Students in a Subject\n"
                "7. Logout\n", staff->staffName);  
                #ifdef DEBUG
                printf("\nDEBUG - 8. View Subject ID's\n");
                #endif             
        printf("\nSelect your option (1-7): ");

        fgets(userInput, MAX_OPTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error, Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);
    
        switch(userSelection) /* Switch case to check input and execute appropriate function */
        {
            case 1:
                change_staff_account_details(staff);
                save_system_state(system);
                break;
            case 2:
                create_new_subject(system, staff);
                save_system_state(system);
                break;
            case 3:
                printf("\nTotal Number Of Subjects: %d\n\n", system->numSubjects);
                printSubjectLinkedList(system->headSubjectNode);
                break;
            case 4:
                create_announcement(system, staff);
                save_system_state(system);
                break;
            case 5:
                mark_assignment(system, staff);
                save_system_state(system);
                break;
            case 6:
                view_students_in_subject(system, staff);
                break;
            case 7:
                printf("Logging out!\n\n");
                break;
#ifdef DEBUG
            case 8:
                printStringLinkedList(staff->subjectsHeadNode, FALSE, FALSE);
                printStringLinkedList(staff->subjectsHeadNode, TRUE, FALSE);
                printStringLinkedList(staff->subjectsHeadNode, TRUE, TRUE);
                break;
#endif
            default:
                printf("Invalid choice\n"); /* Error check to inhibit invalid inputs */
        } 
    } while(userSelection != 7);
}

/*******************************************************************************
 * Description: Allows the staff to change their name and password on the system.
 * inputs: Pointer to the current staff
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void change_staff_account_details(StaffTypePtr staff) 
{
	char userName[MAX_NAME_LEN + 1] = "\0";
    char userPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
	char prompt[PROMPT_LENGTH + 1];
    int cancel;

    printf("--- Change Account Details ---\n\n");

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter new full name: ");
    cancel = validateInput(MAX_NAME_LEN, userName, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter new password (Max 8 Char): ");
    cancel = validateInput(MAX_PASSWORD_LENGTH, userPassword, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    printf("\nYou have successfully changed your details: \n");

    
    printf("Staff New Name: %s\n", userName);
    printf("Staff New Password: %s\n\n", userPassword);

    strcpy(staff->staffName, userName);
    strcpy(staff->password, userPassword);
}

/*******************************************************************************
 * Description: Allows a staff to create a new subject in the system.
 * inputs: Pointer to the current system, pointer to the current staff
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void create_new_subject(UTSCLISystemType* system, StaffTypePtr staff) 
{
    char subjectID[SUBJECT_ID_LEN + 1] = "\0";
	char subjectName[MAX_NAME_LEN + 1] = "\0";
	char prompt[PROMPT_LENGTH + 1];
	int cancel, IDCheck;

	printf("--- Creating new subject ---\n\n");

    IDCheck = FALSE;

    do
    {
        /* prompt created and passed to the getStrAndCh function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the preivous menu */
        sprintf(prompt,"Enter Subject ID (5 Characters): ");
        cancel = validateInput(STUDENT_ID_LEN, subjectID, prompt, TRUE);
        if(cancel == RETURN_TO_MENU)
        {
            printf("\n");
            return;
        }

        /*Check to see the length of the subject ID*/
        if(strlen(subjectID) != 5)
        {
            printf("\nError: Please enter an ID of Length 5 Characters\n\n");  
            continue;
        }

        SubjectTypePtr subject = searchForSubjectAndReturn(system->headSubjectNode, subjectID);

        /*Check to see if the subject number already exists*/
        if(subject != NULL)
        {
            printf("\nError: ID already in use\n\n");   
            IDCheck = FALSE;           
        }
        else
        {
            IDCheck = TRUE;
        }
    }
    while(IDCheck == FALSE);

    /* prompt created and passed to the getStrAndCh function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter subject name: ");
    cancel = validateInput(MAX_NAME_LEN, subjectName, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }   

    printf("\nYou have successfuly created a new subject: \n");
    printf("Subject ID: %s\n", subjectID);
    printf("Subject Name: %s\n\n", subjectName);

    SubjectTypePtr newSubjectNode = createSubjectNode(subjectID, subjectName);

    addSubjectNodeToList(system, newSubjectNode);

    StringTypePtr newString = createStringNode(subjectID);
    addStringNodeToList(&staff->subjectsHeadNode, newString);
    staff->numOfSubjectsManaged++;
    
}

/*******************************************************************************
 * Description: Allows staff to see students in their subjects.
 * inputs: Pointer to the current system, pointer to the current staff
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void view_students_in_subject(UTSCLISystemType* system, StaffTypePtr staff) 
{
    char userInput[MAX_SELECTION_INPUT + EXTRA_SPACES];
    int userSelection;
    int IDCheck = TRUE;

    if(staff->numOfSubjectsManaged == 0) /* Check if staff has any subjects */
    {
        printf("\nError, You currently don't manage any subjects\n");
        printf("Please create one first\n\n");
        return;
    }

    do
    {
        printStringLinkedList(staff->subjectsHeadNode, TRUE, TRUE);
        printf("\nSelect your option (1-%d): ", staff->numOfSubjectsManaged);
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input length*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error, Input a number between (1-%d)\n\n", staff->numOfSubjectsManaged);
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);

        /* Check for valid input range*/
        if(userSelection > 0 && userSelection <= staff->numOfSubjectsManaged)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error, Input a number between (1-%d)\n\n", staff->numOfSubjectsManaged);
            IDCheck = FALSE;
        }

    } while(IDCheck == FALSE);

    printf("\n");

    StringTypePtr selectedStringPtr = getStringLinkedListNode(staff->subjectsHeadNode, userSelection);

    printStudentDetailsInSubject(system, selectedStringPtr->string, FALSE);

    printf("\n");

}

/*******************************************************************************
 * Description: Allows staff to change the mark attribute of an assignment.
 * inputs: Pointer to the current system, pointer to the current staff.
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void mark_assignment(UTSCLISystemType* system, StaffTypePtr staff) 
{
    char studentID[STUDENT_ID_LEN + 1] = "\0";
    char userInput[MAX_SELECTION_INPUT + EXTRA_SPACES];
    char fileDestination[MAX_SUBJECT_NAME_LEN + 1] = DOWNLOAD_DIR;
    char fileSource[MAX_SUBJECT_NAME_LEN + 1] = UPLOAD_DIR;
    float mark;
    int userSelection;
    int IDCheck = TRUE;
    char prompt[PROMPT_LENGTH + 1];
	int cancel, i;
    int assignmentNumber = 0;
    FILE *file;
    StudentTypePtr selectedStudent;

    if(staff->numOfSubjectsManaged == 0)
    {
        printf("\nError, You currently don't manage any subjects\n");
        printf("Please create one first\n\n");
        return;
    }

    /* Check to see if the directory exists*/
    if ((file = fopen(DOWNLOAD_DIR, "w")))
    {
        printf("Error! Please create directory in folder named \"downloaded\" \n\n");
        remove(DOWNLOAD_DIR); /* Remove file that fOpen creates*/
        fclose(file);
        return;
    }

    do
    {
        printStringLinkedList(staff->subjectsHeadNode, TRUE, TRUE);
        printf("\nSelect your option (1-%d): ", staff->numOfSubjectsManaged);
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error, Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);

        if(userSelection > 0 && userSelection <= staff->numOfSubjectsManaged)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error, Input a number between (1-%d)", staff->numOfSubjectsManaged);
            IDCheck = FALSE;
        }

    } while(IDCheck == FALSE);

    printf("\n");

    StringTypePtr selectedStringPtr = getStringLinkedListNode(staff->subjectsHeadNode, userSelection);

    int printStudentResult = printStudentDetailsInSubject(system, selectedStringPtr->string, FALSE);

    /* Previous function return a failed result and thus there is no point in continuing*/
    if(printStudentResult == 1)
    {
        return;
    }

    /* Reset ID check for reuse*/
    IDCheck = FALSE;

    do
    {
        /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
        sprintf(prompt,"\nEnter Student ID to mark: \n\n");
        cancel = validateInput(STUDENT_ID_LEN, studentID, prompt, TRUE);
        if(cancel == RETURN_TO_MENU)
        {
            printf("\n");
            return;
        }

        selectedStudent = searchForStudentAndReturn(system->headStudentNode, studentID);

        /* Make sure the student does exist*/
        if(selectedStudent == NULL)
        {
            printf("Error, Student ID does not exist!\n\n");
            IDCheck = FALSE;
        }
        else
        {
            IDCheck = TRUE;
        }
    } while(IDCheck == FALSE);

    /* Find what assigment in the student assigment array is the one relivent to this subject*/
    for(i = 0; i < selectedStudent->numOfAssignments; i++)
    {
        if(strcmp(selectedStringPtr->string, selectedStudent->assignments[i].subject) == 0)
        {
            assignmentNumber = i;
            break;
        }
    }
    
    /* Create source path*/
    strcat(fileSource, "/");
    strcat(fileSource, selectedStudent->assignments[assignmentNumber].filename);

    /* Decompress the file*/
    extract(fileSource, fileSource);

    /* Create destination path*/
    strcat(fileDestination, "/");
    strcat(fileDestination, selectedStudent->assignments[assignmentNumber].filename);
    strcat(fileDestination, FILE_EXT);

    /* move file*/
    rename(fileSource, fileDestination);

    printf("Assignment has been downloaded to downloaded folder!\n");
    printf("Assignment file name is: %s\n", selectedStudent->assignments[assignmentNumber].filename);

    /* Reset ID check for reuse*/
    IDCheck = FALSE;

    do
    {
        printf("\nEnter students mark (0 - 100): ");
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input length*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error, Input a number between (0-100)");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%f", &mark);

        /* Check for valid input range*/
        if(mark >= 0 && mark <= 100)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error, Input a number between (0-100)");
            IDCheck = FALSE;
        }

    } while(IDCheck == FALSE);

    selectedStudent->assignments[assignmentNumber].mark = mark;

    printf("\n\nMark of: %f, Given to Student ID : %s, For Assignment: %s\n\n", mark, 
        selectedStudent->studentID, selectedStudent->assignments[assignmentNumber].assignmentName);
}

/*******************************************************************************
 * Description: Allows a staff to create a new announcement for a subject
 * inputs: Pointer to the current system, pointer to the current staff
 * outputs: None
 * Author: Hakan Day
*******************************************************************************/
void create_announcement(UTSCLISystemType* system, StaffTypePtr staff) 
{
    char userInput[MAX_SELECTION_INPUT + EXTRA_SPACES];
    char annoucement[MAX_STRING_LENGTH + EXTRA_SPACES];
    char annoucementMessage[MAX_STRING_LENGTH + EXTRA_SPACES];
    char prompt[PROMPT_LENGTH + 1];
    int userSelection;
    int IDCheck = TRUE;
    int cancel;

    if(staff->numOfSubjectsManaged == 0) /* Check if staff has any subjects */
    {
        printf("\nError, You currently don't manage any subjects\n");
        printf("Please create one first\n\n");
        return;
    }

    printf("Please select subject to create annoucement for.\n\n");

    do
    {
        printStringLinkedList(staff->subjectsHeadNode, TRUE, TRUE);
        printf("\nSelect your option (1-%d): ", staff->numOfSubjectsManaged);
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error, Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);

        if(userSelection > 0 && userSelection <= staff->numOfSubjectsManaged)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error, Input a number between (1-%d)", staff->numOfSubjectsManaged);
            IDCheck = FALSE;
        }

    } while(IDCheck == FALSE);

    SubjectTypePtr selectedSubject = getSubjectLinkedListNode(system->headSubjectNode, userSelection);

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"\nEnter message for annoucement: \n\n");
    cancel = validateInput(MAX_STRING_LENGTH, annoucementMessage, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    sprintf(annoucement, "Subject ID: %s - Subject Name: %s - %s",  
        selectedSubject->subjectID, selectedSubject->subjectName, annoucementMessage);

    addAnnoucementsToStudentsInSubject(system, selectedSubject, annoucement);

    printf("You have successfully created the annoucment: \n\n%s \n\n", annoucement); /* Success message */
}
