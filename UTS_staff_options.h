/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * 
*******************************************************************************/


#ifndef UTS_STAFF_OPTIONS_H
#define UTS_STAFF_OPTIONS_H

/* Function prototypes. */
void staff_main_menu(UTSCLISystemType* system, StaffTypePtr student);
void create_new_subject(UTSCLISystemType* system, StaffTypePtr staff);
void view_students_in_subject(UTSCLISystemType* system, StaffTypePtr staff);
void mark_assignment(UTSCLISystemType* system, StaffTypePtr staff);
void create_announcement(UTSCLISystemType* system, StaffTypePtr staff);
void change_staff_account_details(StaffTypePtr staff);

#endif