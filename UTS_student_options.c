/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 * 
 * This file holds all of the student menu and the options that that menu can 
 * navigate too
*******************************************************************************/

/* Included C File Headers */
#include "UTS_CLI.h"
#include "UTS_staff_options.h"
#include "UTS_student_options.h"
#include "UTS_system_options.h"
#include "UTS_utility.h"

/*******************************************************************************
 * Description: Prints the initial student menu and requests an input.
 * inputs: Pointer to the current system state, pointer to the current student
 * outputs: None
 * Author: Berkan Yuksel
*******************************************************************************/
void main_student_menu(UTSCLISystemType* system, StudentTypePtr student) 
{
    char userInput[MAX_OPTION_INPUT + EXTRA_SPACES];
    int userSelection;

    do 
    {
	/* Print menu screen here */
        printf("Welcome, %s\n"
                "1. Change Account Details\n"
                "2. Upload an Assignment\n"
                "3. View Grades\n"
                "4. Enrol in a Class\n"
                "5. View All Announcements\n"
                "6. Clear All Announcements\n"
                "7. Logout\n", student->studentName);
        printf("\nSelect your option (1-7): ");
                
        fgets(userInput, MAX_OPTION_INPUT + EXTRA_SPACES, stdin); /* Get single bit input from user for char choice */

        printf("\n");

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error - Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);
    
        switch(userSelection) /* Read choice to determine which function to execute, then break the switch statement */
        {
            case 1:
                change_student_account_details(student);
                save_system_state(system);
                break;
            case 2:
                upload_assignment_logic(student);
                save_system_state(system);
                break;
            case 3:
                view_marks_logic(student);
                break;
            case 4:
                enrol_in_class_logic(system, student);
                save_system_state(system);
                break;
            case 5:
                view_announcements(student);
                break;
            case 6:
                clear_announcements_logic(student);
                save_system_state(system);
                break;
            case 7:
                printf("Logging out\n\n");
                break;
            default:
                printf("Invalid choice\n"); /* Error check measure to ensure valid integer is given */ 
        } 
    } while(userSelection != 7);
}

/*******************************************************************************
 * Description: Update the user's name and password with new inputs.
 * inputs: Pointer to the current student
 * outputs: None
 * Author: Hakan Day
*******************************************************************************/
void change_student_account_details(StudentTypePtr student) 
{
	char userName[MAX_NAME_LEN + 1] = "\0";
    char userPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
    char prompt[PROMPT_LENGTH + 1];
    int cancel;

    printf("--- Change Account Details ---\n\n");

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter new full name: ");
    cancel = validateInput(MAX_NAME_LEN, userName, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter new password (Max 8 Char): ");
    cancel = validateInput(MAX_PASSWORD_LENGTH, userPassword, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    printf("\nYou have successfully changed your details: \n");

    
    printf("Student New Name: %s\n", userName);
    printf("Student New Password: %s\n\n", userPassword);

    strcpy(student->studentName, userName); /* Pass the username and password to the student pointer's attribute locations */
    strcpy(student->password, userPassword);
}

/*******************************************************************************
 * Description: Takes assignment file from "uploaded" folder and process submission.
 * inputs: Pointer to the current student
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void upload_assignment_logic(StudentTypePtr student) 
{
    char fileName[MAX_SUBJECT_NAME_LEN + 1] = "\0";
    char fileNameNew[MAX_SUBJECT_NAME_LEN + 1] = "\0";
    char destination[MAX_SUBJECT_NAME_LEN + 1] = UPLOAD_DIR;
    char assignmentName[MAX_SUBJECT_NAME_LEN + 1] = "\0";
    char prompt[PROMPT_LENGTH + 1];
    char userInput[MAX_SELECTION_INPUT + EXTRA_SPACES];
    char* token;
    int userSelection;
    int cancel;
    int fileCheck = FALSE;
    int IDCheck = FALSE;
    int i;
    FILE *file;

    if(student->numOfEnrolledClasses == 0)
    {
        printf("Error - You have no enrolled subjects to upload to.\n\n");
        return;
    }

    /* Check to see if the directory exists*/
    if ((file = fopen(UPLOAD_DIR, "w")))
    {
        printf("Error - Please create directory in folder named \"uploaded\" \n\n");
        remove(UPLOAD_DIR); /* Remove file that fopen creates*/
        fclose(file);
        return;
    }

    do
    {
        printf("Select what subject this assignment is for: \n\n");

        for(i = 0; i < student->numOfEnrolledClasses; i++)
        {
            printf("%d. %s\n", i + 1, student->enroledClasses[i]);           
        }

        printf("\n\nSelect your option (1-%d): ", student->numOfEnrolledClasses);
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error - Input a number between (1-%d)\n\n", student->numOfEnrolledClasses);
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);

        /* Error check to make sure the user selection is valid*/
        if(userSelection > 0 && userSelection <= student->numOfEnrolledClasses)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error - Input a number between (1-%d)\n\n", student->numOfEnrolledClasses);
            IDCheck = FALSE;
        }
    } while(IDCheck == FALSE);

    /* Check that the student has not already upload an assignnet for that subject*/
    for(i = 0; i < student->numOfAssignments; i++)
    {
        if(strcmp(student->assignments[i].subject, student->enroledClasses[userSelection - 1]) == 0)    
        {
            printf("Error - Assignment already submitted for %s\n", student->enroledClasses[userSelection - 1]);
            printf("Only 1 submission allowed!\n\n");
            return;
        }    
    }
    
    printf("\n\nPlease copy the assignment into the programs directory\n");
    printf("Assignment must be in .txt format\n\n");

    do
    {
        /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
        sprintf(prompt,"Enter the file name (eg Assignment1.txt): ");
        cancel = validateInput(MAX_SUBJECT_NAME_LEN, fileName, prompt, FALSE);
        if(cancel == RETURN_TO_MENU)
        {
            printf("\n");
            return;
        }

        /* Check the files does exist*/
        file = fopen(fileName, "r");
        if (file == NULL)
        {
            printf("Error - File does not exist.\n\n");
            fileCheck = FALSE;
        }
        else
        {
            /* Check to see if file is empty */
            fseek (file, 0, SEEK_END);
            long size = ftell(file);
            if (0 == size) 
            {
                printf("Error - File is empty!\n\n");
                return;
            }
            fclose(file);

            fileCheck = TRUE;
        }

    } while(fileCheck == FALSE);

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
    * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter assignment name: ");
    cancel = validateInput(MAX_SUBJECT_NAME_LEN, assignmentName, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    /* Remove the .txt on the end of the file */
    token = strtok(fileName, DELIM2);
    strcpy(fileNameNew, token);

    /* Rebuild original file name */
    strcat(fileName, FILE_EXT);

    /*Compress the file */
    compress(fileName, fileNameNew);

    #ifdef DEBUG
        printf("fileName: %s\n\n", fileName);
    #endif

    /* Remove the leftover file*/
    remove(fileName);

    /* Create destination path */
    strcat(destination, "/");
    strcat(destination, fileNameNew);

    #ifdef DEBUG
        printf("DEBUG - file destination: %s", destination);
    #endif

    /* Move file to destination */
    rename(fileNameNew, destination);

    /* Set all assignemnt details for the student */
    strcpy(student->assignments[student->numOfAssignments].subject, student->enroledClasses[userSelection - 1]);
    strcpy(student->assignments[student->numOfAssignments].assignmentName, assignmentName);
    strcpy(student->assignments[student->numOfAssignments].filename, fileNameNew);
    student->assignments[student->numOfAssignments].mark = -1;

    student->numOfAssignments++;

    printf("\nAssignment successfully uploaded\n\n");
}

/*******************************************************************************
 * Description: Clears the student's list of announcements from memory.
 * inputs: Pointer to the current student
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void clear_announcements_logic(StudentTypePtr student) 
{
    freeStringLinkedList(&student->announcementsHeadNode); /* Reset head of linked list */
    student->numOfAnnoucements = 0; /* Reinitialise number of announcements to 0 */
}

/*******************************************************************************
 * Description: Allows a student to enrol into an existing subject on the system
 * inputs: Pointer to the current system, pointer to the current student
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void enrol_in_class_logic(UTSCLISystemType* system, StudentTypePtr student) 
{
    char userInput[MAX_SELECTION_INPUT + EXTRA_SPACES];
    int userSelection;
    int IDCheck = TRUE;
    int i;

    /* If no subject exist return*/
    if(system->numSubjects == 0)
    {
        printf("\nError - No subjects available\n");
        printf("Please wait for a teacher to create one\n\n");
        return;
    }

    /* If student is already enrolled in the max amount of subjects return*/
    if(student->numOfEnrolledClasses == MAX_ALLOWED_SUBJECTS)
    {
        printf("\nError - You are enrolled in your max subjects\n\n");
        return;
    }

    do
    {
        printSubjectsForStudentToEnrol(system->headSubjectNode);
        printf("\nSelect your option (1-%d): ", system->numSubjects);
                
        fgets(userInput, MAX_SELECTION_INPUT + EXTRA_SPACES, stdin);

		/*Check for valid input*/
		if (userInput[strlen(userInput)-1] != '\n')
		{
			printf("Error - Input a single number 1-3\n\n");
			readRestOfLine();
            continue; /*Skip the rest of the loop code*/
		}

        sscanf(userInput, "%d", &userSelection);

        if(userSelection > 0 && userSelection <= system->numSubjects)
        {
            IDCheck = TRUE;
        }
        else
        {
            printf("Error - Input a number between (1-%d)\n\n", system->numSubjects);
            IDCheck = FALSE;
        }

    } while(IDCheck == FALSE);

    printf("\n");

    SubjectTypePtr selectedSubject = getSubjectLinkedListNode(system->headSubjectNode, userSelection);

    /* Check to see if the student has already enroled in selected subject and if so return*/
    for(i = 0; i < student->numOfEnrolledClasses; i++)
    {
        if(strcmp(selectedSubject->subjectID, student->enroledClasses[i]) == 0)
        {
            printf("Error - You have already enrolled in that subject\n\n");
            return;
        }
    }

    StringTypePtr studentID = createStringNode(student->studentID);

    /* Set enroled variables in the system*/
    addStringNodeToList(&selectedSubject->studentIDsEnrolledHead, studentID);
    selectedSubject->numOfEnrolledStudents++;

    /* Set enroled variables in the student*/
    strcpy(student->enroledClasses[student->numOfEnrolledClasses], selectedSubject->subjectID);
    student->numOfEnrolledClasses++;


    printf("\nCongratulations, you have successfully enrolled in %s - %s\n\n", 
        selectedSubject->subjectID, selectedSubject->subjectName);


}

/*******************************************************************************
 * Description: Prints a student's grades onto the screen in a table format.
 * inputs: Pointer to the current student
 * outputs: None
 * Author: Berkan Yuksel
*******************************************************************************/
void view_marks_logic(StudentTypePtr student) 
{
	if(student->numOfAssignments == 0) { /* Check if student has submitted any work */
		printf("Note - You have not submitted any assignments\n\n"); /* Error message */
		return;
	}

	int i;
    printf("Note - Assignments with mark -1 are not yet Marked\n\n");
    printf("Subject ID| Assignment Name | Mark\n"
        "----------------------------------\n");
	for(i = 0; i < student->numOfAssignments; i++) /* Print list of assignments and marks until end of assignments */
    {
       
        printf("%-s         %-s             | %-.1f\n",
        student->assignments[i].subject,
        student->assignments[i].assignmentName,
        student->assignments[i].mark);
	}	
    printf("\n");
}

/*******************************************************************************
 * Description: Prints a linked list of announcements belonging to the student.
 * inputs: Pointer to the current student
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void view_announcements(StudentTypePtr student) 
{
	if(student->numOfAnnoucements == 0) /* Check if student has any announcements */
    {
        printf("Note - You currently do not have any announcements\n\n"); /* Error message */
        return;
    }

    printStringLinkedList(student->announcementsHeadNode, TRUE, FALSE);	/* Print list of announcements until end of announcements linked list */
}
