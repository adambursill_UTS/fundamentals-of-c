/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * 
*******************************************************************************/



#ifndef UTS_STUDENT_OPTIONS_H
#define UTS_STUDENT_OPTIONS_H

/* Function prototypes. */
void main_student_menu(UTSCLISystemType* system, StudentTypePtr student);
void change_student_account_details(StudentTypePtr student);
void upload_assignment_logic(StudentTypePtr student);
void clear_announcements_logic(StudentTypePtr student);
void enrol_in_class_logic(UTSCLISystemType* system, StudentTypePtr student);
void view_marks_logic(StudentTypePtr student);
void view_announcements(StudentTypePtr student);


#endif