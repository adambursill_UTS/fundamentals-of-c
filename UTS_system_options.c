/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * This files holdes the main menu optioned and the functions that are called
 * from it. Note loop is not in main menu file as this is controled from the 
 * main function
*******************************************************************************/


/* Included C File Headers */
#include "UTS_CLI.h"
#include "UTS_staff_options.h"
#include "UTS_student_options.h"
#include "UTS_system_options.h"
#include "UTS_utility.h"

/*******************************************************************************
 * Description: Prints the main menu for the system's initial screen.
 * inputs: None
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void print_main_menu() 
{
    printf("Welcome to UTS CLI\n");
    printf("1. To Log In\n");
    printf("2. To Register\n");
    printf("3. To Exit\n");
    

    #ifdef DEBUG
        printf("DEBUG - 4. To Print Staff Linked List\n");
        printf("DEBUG - 5. To Print Student Linked List\n");
        printf("DEBUG - 6. To Print Subject Linked List\n");
    #endif

    printf("\nSelect your option (1-3): ");
}

/*******************************************************************************
 * Description: Allows system to register a new user to the system.
 * inputs: Pointer to the current system
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void register_new_user(UTSCLISystemType* system) 
{
    char userID[STUDENT_ID_LEN + 1] = "\0"; /* Uses student ID as it is the largest*/
	char userName[MAX_NAME_LEN + 1] = "\0";
    char userPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
    char encryptedPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
	char prompt[PROMPT_LENGTH + 1];
	int cancel, IDCheck;

    IDCheck = FALSE;

	printf("--- Registering new user ---\n");

    do
    {
        /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the preivous menu */
        sprintf(prompt,"Enter user ID (6 or 8 Numbers): ");
        cancel = validateInput(STUDENT_ID_LEN, userID, prompt, TRUE);
        if(cancel == RETURN_TO_MENU) /*Return the current menu*/
        {
            printf("\n"); 
            return;
        }

        if(strlen(userID) == STUDENT_ID_LEN || strlen(userID) == STAFF_ID_LEN)
        {
            IDCheck = TRUE;
            /*Now check to see if the ID's are already in use*/
            if(strlen(userID) == STUDENT_ID_LEN)
            {
                StudentTypePtr student = searchForStudentAndReturn(system->headStudentNode, userID);

                /*Check to see if the student number already exists*/
                if(student != NULL)
                {
                    printf("\nError - ID already in use\n\n");   
                    IDCheck = FALSE;           
                }
            }
            else
            {
                StaffTypePtr staff = searchForStaffAndReturn(system->headStaffNode, userID);

                /*Check to see if the staff number already exists*/
                if(staff != NULL)
                {
                    printf("\nError - ID already in use\n\n");            
                    IDCheck = FALSE;  
                }
            }         
        }
        else
        {
            printf("Error - Please enter 6 or 8 numbers\n\n");
        }
    }
    while(IDCheck == FALSE);


	
    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter full name: ");
    cancel = validateInput(MAX_NAME_LEN, userName, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter password (Max 8 Char): ");
    cancel = validateInput(MAX_PASSWORD_LENGTH, userPassword, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }


    /* Encrypt password and story the encripted version */
    encrypt_password(userPassword, encryptedPassword);

    printf("\nYou have successfully registered: \n");

    if(strlen(userID) == STUDENT_ID_LEN)
    {
        printf("Student ID: %s\n", userID);
	    printf("Student Name: %s\n\n", userName);

        StudentTypePtr newStudent = createStudentNode(userID, userName, encryptedPassword);

        addStudentNodeToList(system, newStudent);

    }
    else
    {
        printf("Staff ID: %s\n", userID);
	    printf("Staff Name: %s\n\n", userName);

        StaffTypePtr newStaff = createStaffNode(userID, userName, encryptedPassword);

        addStaffNodeToList(system, newStaff);
    }

}

/*******************************************************************************
 * Description: Allows the system to log into a registered user account.
 * inputs: Pointer to the current system
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void login_user(UTSCLISystemType* system) 
{
    char userID[STUDENT_ID_LEN + 1] = "\0"; /* Uses student ID as it is the largest*/
    char userPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
    char encryptedPassword[MAX_PASSWORD_LENGTH + 1] = "\0";
    char prompt[PROMPT_LENGTH + 1];
	int cancel;

    printf("--- User Log In ---\n");

	/* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
	 * constant then the function will stop here and return to the preivous menu */
	sprintf(prompt,"Enter user ID (6 or 8 Numbers): ");
	cancel = validateInput(STUDENT_ID_LEN, userID, prompt, TRUE);
	if(cancel == RETURN_TO_MENU)
	{
		printf("\n");
		return;
	}

	
    /* prompt created and passed to the validateInput function, if the function returns the RETURN_TO_MENU
        * constant then the function will stop here and return to the main menu */
    sprintf(prompt,"Enter password: ");
    cancel = validateInput(MAX_NAME_LEN, userPassword, prompt, FALSE);
    if(cancel == RETURN_TO_MENU)
    {
        printf("\n");
        return;
    }

    /* Encrypt password to use for comparing to their system password */
    encrypt_password(userPassword, encryptedPassword);

    if(strlen(userID) == STUDENT_ID_LEN)
    {
        StudentTypePtr student = searchForStudentAndReturn(system->headStudentNode, userID);
        /*Check to see if the student member exists and their password does not match*/
        if(student == NULL || strcmp(student->password, encryptedPassword) != 0)
        {
            printf("\nError - ID or Password Incorrect\n\n");
            return;
        }
        else
        {
            /*Login and proceed to the student main menu*/
            printf("Login successful\n\n");
            main_student_menu(system, student);
        }
    }
    else
    {
        StaffTypePtr staff = searchForStaffAndReturn(system->headStaffNode, userID);
        /*Check to see if the staff member exists and their password does not match*/
        if(staff == NULL || strcmp(staff->password, encryptedPassword) != 0)
        {
            printf("\nError - ID or password incorrect\n\n");
            return;
        }
        else
        {
            /*Login and proceed to the staff main menu*/
            printf("Login successful\n\n");
            staff_main_menu(system, staff);
        }
    }            
}
