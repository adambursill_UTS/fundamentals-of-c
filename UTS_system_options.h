/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 *
 * 
*******************************************************************************/


#ifndef UTS_SYSTEM_OPTIONS_H
#define UTS_SYSTEM_OPTIONS_H

/* Function prototypes. */
void print_main_menu();
void register_new_user(UTSCLISystemType* system);
void login_user(UTSCLISystemType* system);


#endif