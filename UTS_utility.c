/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 * This file holds all of the utility function for the prgram, including but 
 * not limited too, linked list creation and destruction, searching and returning
 * print. System saveing and loading. Compression and encryption
 * 
*******************************************************************************/

/* Included C File Headers */
#include "UTS_CLI.h"
#include "UTS_staff_options.h"
#include "UTS_student_options.h"
#include "UTS_system_options.h"
#include "UTS_utility.h"

/*******************************************************************************
 * Description: This function loads a saved system's students, staff and subjects
   into memory.
 * inputs: A pointer to the current CLI system
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void load_system_state(UTSCLISystemType* system) 
{
    char userIn[MAX_FILE_INPUT_LENGTH];
    FILE* fp = NULL;
    char* token;
	int i;

	/*First load staff data into the system*/

    fp = fopen(STAFF_DB, "r"); /* Open a file for reading */
    /* If there is a read error return, eg file does not exsit*/
    if (fp == NULL)
    {
        printf("No data files found, program in new state\n\n");
		return;
	}
	/* When not debugging files will be encrypted*/
	#ifndef DEBUG

		/* Decrypt files to allow reading */
		decrypt_file(STAFF_DB, ENCRYPTION_KEY);
		decrypt_file(STUDENT_DB, ENCRYPTION_KEY);
		decrypt_file(SUBJECT_DB, ENCRYPTION_KEY);

	#endif

	StaffTypePtr staffTemp; /*Temp staff node to hold all the staff values*/

	fgets(userIn, MAX_FILE_INPUT_LENGTH, fp);

	#ifdef DEBUG
		printf("DEBUG - total saved staff = %s\n\n", userIn);
	#endif

    /* Loop through all the lines in the file and tokenise via the delim "|"" 
    *  seperate the values for each Staff member
    */
	  
    while (fgets(userIn, MAX_FILE_INPUT_LENGTH, fp) != NULL)
    {
		staffTemp = createStaffNode("", "", ""); /* Initalise empty node for storing values*/
        
		token = strtok(userIn, DELIM); /* Tokenise input by delim*/
        strcpy(staffTemp->staffID, token);

		token = strtok(NULL, DELIM);
        strcpy(staffTemp->staffName, token);

		token = strtok(NULL, DELIM);
        strcpy(staffTemp->password, token);

		token = strtok(NULL, DELIM);
        staffTemp->numOfSubjectsManaged = atoi(token); /*atoi converts a string to an int*/

		 /*Now loop throught all subjects managed to add them to the staff memeber*/
		for(i = 0; i < staffTemp->numOfSubjectsManaged; i++)
		{
			StringTypePtr stringTemp = createStringNode("");

			token = strtok(NULL, DELIM);
			if(i + 1 == staffTemp->numOfSubjectsManaged)
			{
				token[strlen(token) - 1] = '\0'; /*last token will keep '\n' char on the end*/
			}
        	strcpy(stringTemp->string, token);

			/* For each string that was tokenised at a new one the staff string head node*/
			addStringNodeToList(&staffTemp->subjectsHeadNode, stringTemp);
		}

		addStaffNodeToList(system, staffTemp);
    }

    fclose(fp);

	/*Second load subject data into the system*/

    fp = fopen(SUBJECT_DB, "r"); /* Open a file for reading */
    /* If there is a read error return, eg file does not exsit*/
    if (fp == NULL)
    {
        printf("Read error\n");
		return;
	}

	SubjectTypePtr subjectTemp; /*Temp subject node to hold all the subject values*/

	fgets(userIn, MAX_FILE_INPUT_LENGTH, fp);

	#ifdef DEBUG
		printf("DEBUG - total saved subjects = %s\n\n", userIn);
	#endif

    /* Loop through all the lines in the file and tokenise via the delim "|"" 
    *  seperate the values for each stubject
    */
	  
    while (fgets(userIn, MAX_FILE_INPUT_LENGTH, fp) != NULL)
    {
		subjectTemp = createSubjectNode("", "");
        
		token = strtok(userIn, DELIM);
        strcpy(subjectTemp->subjectID, token);

		token = strtok(NULL, DELIM);
        strcpy(subjectTemp->subjectName, token);

		token = strtok(NULL, DELIM);
        subjectTemp->numOfEnrolledStudents = atoi(token); /*atoi converts a string to an int*/

		 /*Now loop throught all enrolled studenets in that subject*/
		for(i = 0; i < subjectTemp->numOfEnrolledStudents; i++)
		{
			StringTypePtr stringTemp = createStringNode("");

			token = strtok(NULL, DELIM);
			if(i + 1 == subjectTemp->numOfEnrolledStudents)
			{
				token[strlen(token) - 1] = '\0'; /*last token will keep '\n' char on the end*/
			}
        	strcpy(stringTemp->string, token);

			addStringNodeToList(&subjectTemp->studentIDsEnrolledHead, stringTemp);
		}

		addSubjectNodeToList(system, subjectTemp);
    }

    fclose(fp);

	/*Third load student data into the system*/

    fp = fopen(STUDENT_DB, "r"); /* Open a file for reading */
    /* If there is a read error return, eg file does not exsit*/
    if (fp == NULL)
    {
        printf("Read error\n");
		return;
	}

	StudentTypePtr studentTemp; /*Temp student node to hold all the subject values*/

	fgets(userIn, MAX_FILE_INPUT_LENGTH, fp);

	#ifdef DEBUG
		printf("DEBUG - total saved students = %s\n\n", userIn);
	#endif

    /* Loop through all the lines in the file and tokenise via the delim "|"" 
    *  seperate the values for each student
    */
	  
    while (fgets(userIn, MAX_FILE_INPUT_LENGTH, fp) != NULL)
    {
		studentTemp = createStudentNode("", "", "");
        
		token = strtok(userIn, DELIM);
        strcpy(studentTemp->studentID, token);

		token = strtok(NULL, DELIM);
        strcpy(studentTemp->studentName, token);

		token = strtok(NULL, DELIM);
        strcpy(studentTemp->password, token);

		token = strtok(NULL, DELIM);
        studentTemp->numOfEnrolledClasses = atoi(token); /*atoi converts a string to an int*/

		/* Loop through all classes that the student is enrolled in*/
		for(i = 0; i < studentTemp->numOfEnrolledClasses; i++)
		{
			token = strtok(NULL, DELIM);
        	strcpy(studentTemp->enroledClasses[i], token);
		}
		
		token = strtok(NULL, DELIM);
        studentTemp->numOfAssignments = atoi(token); /*atoi converts a string to an int*/

		/* Loop through all assignments that the student has submitted*/
		for(i = 0; i < studentTemp->numOfAssignments; i++)
		{
			token = strtok(NULL, DELIM);
        	strcpy(studentTemp->assignments[i].subject, token);

			token = strtok(NULL, DELIM);
        	strcpy(studentTemp->assignments[i].assignmentName, token);

			token = strtok(NULL, DELIM);
        	strcpy(studentTemp->assignments[i].filename, token);

			token = strtok(NULL, DELIM);
        	studentTemp->assignments[i].mark = atof(token);
		}

		token = strtok(NULL, DELIM);
        studentTemp->numOfAnnoucements = atoi(token); /*atoi converts a string to an int*/

		 /*Now loop throught all annoucements for the student*/
		for(i = 0; i < studentTemp->numOfAnnoucements; i++)
		{
			StringTypePtr stringTemp = createStringNode("");

			token = strtok(NULL, DELIM);
			if(i + 1 == studentTemp->numOfAnnoucements)
			{
				token[strlen(token) - 1] = '\0'; /*last token will keep '\n' char on the end*/
			}
        	strcpy(stringTemp->string, token);

			addStringNodeToList(&studentTemp->announcementsHeadNode, stringTemp);
		}

		addStudentNodeToList(system, studentTemp);
    }

    fclose(fp);

	#ifndef DEBUG

		encrypt_file(STAFF_DB, ENCRYPTION_KEY);
		encrypt_file(STUDENT_DB, ENCRYPTION_KEY);
		encrypt_file(SUBJECT_DB, ENCRYPTION_KEY);

	#endif
}

/*******************************************************************************
 * Description: This function saves the system's current list of students,
   staff and subjects to database files.
 * inputs: A pointer to the current CLI system
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void save_system_state(UTSCLISystemType* system) 
{
    FILE* fp = NULL;
	int i;

	fp = fopen(STAFF_DB, "w"); /*Open file for writing*/

    /* If there is a file write error, eg could be locked*/
    if (fp == NULL)
    {
        printf("Write error\n");
        return;
    }

	/* First save all of the staff linked list*/

	StaffTypePtr staffCurrent;
	staffCurrent = system->headStaffNode;

	/*First line in the saved file will be the total number of staff that are saved*/
	fprintf(fp, "%d\n", system->numStaff);

	/* Loop through all staff nodes and print out for each of them to file*/
	while (staffCurrent != NULL)
	{
		fprintf(fp, "%s|%s|%s|%d", staffCurrent->staffID, staffCurrent->staffName, 
			staffCurrent->password, staffCurrent->numOfSubjectsManaged);
		/*print out string linked list to file*/
		save_string_linked_list_to_file(fp, staffCurrent->subjectsHeadNode); 
		staffCurrent = staffCurrent->nextStaffNode;
		fprintf(fp, "\n");
	}

	fclose(fp);

	/* Now save all of the subjects linked list*/

	fp = fopen(SUBJECT_DB, "w"); /*Open file for writing*/

    /* If there is a file write error, eg could be locked*/
    if (fp == NULL)
    {
        printf("Write error\n");
        return;
    }

	SubjectTypePtr subjectCurrent;
	subjectCurrent = system->headSubjectNode;

	/* First line in the saved file will be the total number of staff that are saved */
	fprintf(fp, "%d\n", system->numSubjects);

	while (subjectCurrent != NULL)
	{
		fprintf(fp, "%s|%s|%d", subjectCurrent->subjectID, subjectCurrent->subjectName,  
			subjectCurrent->numOfEnrolledStudents);
		save_string_linked_list_to_file(fp, subjectCurrent->studentIDsEnrolledHead);
		subjectCurrent = subjectCurrent->nextSubjectNode;
		fprintf(fp, "\n");
	}

	fclose(fp);

	fp = fopen(STUDENT_DB, "w"); /*Open file for writing*/

    /* If there is a file write error, eg could be locked*/
    if (fp == NULL)
    {
        printf("Write error\n");
        return;
    }

	/* Now save all of the students linked list*/

	StudentTypePtr studentCurrent;
	studentCurrent = system->headStudentNode;

	/* First line in the saved file will be the total number of staff that are saved */
	fprintf(fp, "%d\n", system->numStudents);

	while (studentCurrent != NULL)
	{
		fprintf(fp, "%s|%s|%s|%d", studentCurrent->studentID, studentCurrent->studentName,
		 	studentCurrent->password, studentCurrent->numOfEnrolledClasses);

		/* For every enrolled class string in the enroled class array save it to file*/
		for(i = 0; i < studentCurrent->numOfEnrolledClasses; i++)
		{
			fprintf(fp, "|%s", studentCurrent->enroledClasses[i]);
		}

		fprintf(fp, "|%d", studentCurrent->numOfAssignments);

		/*Save all details of assignemnts in the assignments array*/
		for(i = 0; i < studentCurrent->numOfAssignments; i++)
		{
			fprintf(fp, "|%s|%s|%s|%f", studentCurrent->assignments[i].subject, 
				studentCurrent->assignments[i].assignmentName, studentCurrent->assignments[i].filename,
				studentCurrent->assignments[i].mark);
		}		

		fprintf(fp, "|%d", studentCurrent->numOfAnnoucements);

		save_string_linked_list_to_file(fp, studentCurrent->announcementsHeadNode);
		studentCurrent = studentCurrent->nextStudentNode;
		fprintf(fp, "\n");
	}

	fclose(fp);

	#ifndef DEBUG

		encrypt_file(STAFF_DB, ENCRYPTION_KEY);
		encrypt_file(STUDENT_DB, ENCRYPTION_KEY);
		encrypt_file(SUBJECT_DB, ENCRYPTION_KEY);

	#endif
}

/*******************************************************************************
 * Description: This function prints out all strings in the string linked list
 * inputs: File pointer of where to print too, head node of the string linked list
 * outputs: None
 * Author: Adam Bursill
*******************************************************************************/
void save_string_linked_list_to_file(FILE* filePtr, StringTypePtr headNode)
{
	StringTypePtr staffCurrent;
	staffCurrent = headNode;
	/* Loop through all nodes and print out each string at that node*/
	while (staffCurrent != NULL)
	{
		fprintf(filePtr, "|%s", staffCurrent->string);
		staffCurrent = staffCurrent->nextStringNode;
	}
}

/*******************************************************************************
 * Description: This file takes an encrypted file and decrypts it into itself.
 * inputs: Name of the file, a key to perform encryption
 * outputs: int (for error handling), overwrites encrypted file
 * Author: Berkan Yuksel
*******************************************************************************/
int decrypt_file(const char * file_name, int key) {
    char * read_buffer = NULL; /* Initialise a read buffer to check the size of file */
	size_t size = 0;
	
	FILE * file_read = fopen(file_name, "r"); /* Create a file pointer for reading */
	
	if(file_read == NULL) { /* Error check to see if file opened correctly */
		#ifdef DEBUG
			printf("DEBUG - Cannot find file to be encrypted.\n");
		#endif
		return 1;
	} else {
		fseek(file_read, 0, SEEK_END); /* Read the size of the file in bytes */
		size = ftell(file_read); /* Hold it in memory */
		
		rewind(file_read); /* Rewind the file pointer so it can be used again */
		
		read_buffer = malloc((size + 1) * sizeof(*read_buffer)); /* Allocate memory for buffer */
		
		fread(read_buffer, size, 1, file_read); /* Read the file into the buffer */
		read_buffer[size] = '\0'; /* Null-terminate the buffer string */
		#ifdef DEBUG
			printf("DEBUG - Encrypted file has been successfully found.\n");
		#endif
	
	}
	
	fclose(file_read); /* Clear the file pointer from memory */
	
	int i = 0;
	int cipher_value;
	char cipher;
	char * cipher_string;
	cipher_string = malloc((size + 1) * sizeof(*read_buffer)); /* Initialise and allocate a cipher string to memory */
	
    while(read_buffer[i] != '\0' && strlen(read_buffer) > i){ /* While the cipher string has not reached its end... */
        cipher_value = ((int)read_buffer[i] + 97 - (key % 26)); /* Typecast an int onto the char in iteration to perform mathematical decryption */
        cipher = (char)(cipher_value);
		cipher_string[i] = cipher; /* Recast the char type to the int, and add to the cipher string */
        i++;
    }
	
	cipher_string[size] = '\0'; /* Null-terminate the cipher string */
	
	free(read_buffer); /* Free the read buffer from memory */
	
	FILE * file_write = NULL; /* Create a file pointer for writing */
	file_write = fopen(file_name, "w");
	
	if(file_write == NULL) { /* Error check to see if file opened correctly */
	#ifdef DEBUG
		printf("DEBUG - Error writing decrypted file.\n");
	#endif
		return 1;
	} else {
		fwrite(cipher_string, size, 1, file_write); /* Write decrypted file */
		#ifdef DEBUG
			printf("DEBUG - Encrypted file has been successfully found.\n");
		#endif
	}
	
	free(cipher_string); /* Free cipher string from memory */
	fclose(file_write); /* Clear the file pointer from memory */
	
	return 0;
}

/*******************************************************************************
 * Description: This file takes a file and encrypts it into itself.
 * inputs: Name of the file, a key to perform encryption
 * outputs: int (for error handling), overwrites encrypted file
 * Author: Berkan Yuksel
*******************************************************************************/
int encrypt_file(const char * file_name, int key) {
	char * read_buffer = NULL; /* Initialise a read buffer to check the size of file */
	size_t size = 0;
	
	FILE * file_read = fopen(file_name, "r"); /* Create a file pointer for reading */
	
	if(file_read == NULL) { /* Error check to see if file opened correctly */
		#ifdef DEBUG
			printf("DEBUG - Cannot find file to be encrypted.\n");
		#endif
		return 1;
	} else {
		fseek(file_read, 0, SEEK_END); /* Read the size of the file in bytes */
		size = ftell(file_read); /* Hold it in memory */
		
		rewind(file_read); /* Rewind the file pointer so it can be used again */
		
		read_buffer = malloc((size + 1) * sizeof(*read_buffer)); /* Allocate memory for buffer */
		
		fread(read_buffer, size, 1, file_read); /* Read the file into the buffer */
		read_buffer[size] = '\0'; /* Null-terminate the buffer string */
		
		#ifdef DEBUG
			printf("DEBUG - Encrypted file has been successfully found.\n");
		#endif
    }
	
    fclose(file_read); /* Clear the file pointer from memory */
	
    int i = 0;
    int cipher_value;
    char cipher;
	char * cipher_string;
	cipher_string = malloc((size + 1) * sizeof(*read_buffer)); /* Initialise and allocate a cipher string to memory */
	
    while(read_buffer[i] != '\0' && strlen(read_buffer) > i){ /* While the cipher string has not reached its end... */
        cipher_value = ((int)read_buffer[i] - 97 + (key % 26)); /* Typecast an int onto the char in iteration to perform mathematical encryption */
        cipher = (char)(cipher_value);
		cipher_string[i] = cipher; /* Recast the char type to the int, and add to the cipher string */
        i++;
    }

	cipher_string[size] = '\0'; /* Null-terminate the cipher string */
	
	free(read_buffer); /* Free the read buffer from memory */
	
	FILE * file_write = NULL; /* Create a file pointer for writing */
	file_write = fopen(file_name, "w");
	
	if(file_write == NULL) { /* Error check to see if file opened correctly */
	#ifdef DEBUG
	    printf("DEBUG - Error writing encrypted file.\n");
	#endif
	    return 1;
    } else {
        fwrite(cipher_string, size, 1, file_write); /* Write encrypted file */
		#ifdef DEBUG
			printf("DEBUG - File encrypted and written successfully.\n");
		#endif
    }
    
    free(cipher_string); /* Free cipher string from memory */
    fclose(file_write); /* Clear the file pointer from memory */

    return 0;
}

/*******************************************************************************
 * Description: This system encrypts a password without saving it to a file.
 * inputs: A password input, an encrypted password output location
 * outputs: An encrypted password
 * Author: Nathan Ha
*******************************************************************************/
void encrypt_password(const char* password, char* encryptedPassword)
{

    int key = 3; /* Initialise cipher variables */
    int i=0;
    int cipher_value;
    char cipher;
    while( password[i] != '\0' && strlen(password) > i) /* While the password has not reached its end... */
	{
		/* Typecast an int onto the char in iteration to perform mathematical encryption */
        cipher_value = ((int)password[i] -97 + key) % 26 + 97; 
        cipher = (char)(cipher_value);
        encryptedPassword[i] = cipher;
        i++;
    }
}

/*******************************************************************************
 * Description: Select the two nodes, the smallest and the second, to establish a Huffman tree call.
 * inputs: HufNode *huf_tree, unsigned int n, int *s1, int *s2
 * outputs: void
 * Author: Haoran Huang
*******************************************************************************/

void select(HufNode *huf_tree, unsigned int n, int *s1, int *s2)
{
	/* Find the smallest */ 
	unsigned int i;
	unsigned long min = ULONG_MAX;
	for(i = 0; i < n; ++i)
		if(huf_tree[i].parent == 0 && huf_tree[i].weight < min)
		{
			min = huf_tree[i].weight;
			*s1 = i;
		}
		huf_tree[*s1].parent=1;   /* Mark this node has been selected */

	/* Find a small time */
	min=ULONG_MAX;
	for(i = 0; i < n; ++i)
		if(huf_tree[i].parent == 0 && huf_tree[i].weight < min)
		{
			min = huf_tree[i].weight;
			*s2 = i;
		}
} 

/*******************************************************************************
 * Description: Building a Huffman tree 
 * inputs: HufNode *huf_tree, unsigned int char_kinds, unsigned int node_num
 * outputs: void
 * Author: Haoran Huang
*******************************************************************************/

void CreateTree(HufNode *huf_tree, unsigned int char_kinds, unsigned int node_num)
{
	unsigned int i;
	int s1, s2;
	for(i = char_kinds; i < node_num; ++i)  
	{ 
		select(huf_tree, i, &s1, &s2);		/* Select the smallest two nodes */
		huf_tree[s1].parent = huf_tree[s2].parent = i; 
		huf_tree[i].lchild = s1; 
		huf_tree[i].rchild = s2; 
		huf_tree[i].weight = huf_tree[s1].weight + huf_tree[s2].weight; 
	} 
}

/*******************************************************************************
 * Description: Generating Huffman coding
 * inputs: HufNode *huf_tree, unsigned char_kinds
 * outputs: void
 * Author: Haoran Huang
*******************************************************************************/

void HufCode(HufNode *huf_tree, unsigned char_kinds)
{
	unsigned int i;
	int cur, next, index;
	char *code_tmp = (char *)malloc(NUM_MAX*sizeof(char));		/* Temporary storage code, up to 256 leaves, encoding length is not more than 255 */
	code_tmp[NUM_MAX-1] = '\0'; 

	for(i = 0; i < char_kinds; ++i) 
	{
		index = NUM_MAX-1;	/* Encoding temporary space index initialization */ 

		/* Reverse traversal from leaf to root */ 
		for(cur = i, next = huf_tree[i].parent; next != 0; 
			cur = next, next = huf_tree[next].parent)  
			if(huf_tree[next].lchild == cur) 
				code_tmp[--index] = '0';	/* left‘0’ */ 
			else 
				code_tmp[--index] = '1';	/* right‘1’ */ 
		/* Dynamically allocate storage space for the ith character encoding */
		huf_tree[i].code = (char *)malloc((NUM_MAX-index)*sizeof(char));			
		strcpy(huf_tree[i].code, &code_tmp[index]);     /* Forward save encoding to the corresponding node in the tree node */ 
	} 
	free(code_tmp);		/* Release coded temporary space */ 
}

/*******************************************************************************
 * Description: Compression function
 * inputs: data.txt
 * outputs: data.Huffman
 * Author: Haoran Huang
*******************************************************************************/
void compress(char *ifname, char *ofname)
{
	unsigned int i, j;
	unsigned int char_kinds;		/* Character type */
	unsigned char char_temp;		/* Temporary 8bits character */ 
	unsigned long file_len = 0;
	FILE *infile, *outfile;
	TmpNode node_temp;
	unsigned int node_num;
	HufTree huf_tree;
	char code_buf[NUM_MAX] = "\0";		/* Pending code buffer */ 
	unsigned int code_len;
	/*
	Dynamically allocate 256 nodes, temporarily store character frequency,
	Release and release immediately after copying to the tree node
	*/
	TmpNode *tmp_nodes =(TmpNode *)malloc(NUM_MAX*sizeof(TmpNode));		

	/* Initialize the staging node */ 
	for(i = 0; i < NUM_MAX; ++i)
	{
		tmp_nodes[i].weight = 0;
		tmp_nodes[i].uch = (unsigned char)i;		/* 256 subscripts of an array correspond to 256 characters */
	}

	/* Traverse the file to get the character frequency */
	infile = fopen(ifname, "rb");
	/* Determine if the input file exists */
	if (infile == NULL)
		return;
	fread((char *)&char_temp, sizeof(unsigned char), 1, infile);		/* Read in a character */ 
	while(!feof(infile))
	{
		++tmp_nodes[char_temp].weight;		/* Count the weight of the corresponding character of the subscript, and use the random access of the array to quickly count the character frequency. */
		++file_len;
		fread((char *)&char_temp, sizeof(unsigned char), 1, infile);		/* Read in a character */ 
	}
	fclose(infile);

	/* Sort, put the frequency to zero, put the last, cull */
	for(i = 0; i < NUM_MAX-1; ++i)           
		for(j = i+1; j < NUM_MAX; ++j)
			if(tmp_nodes[i].weight < tmp_nodes[j].weight)
			{
				node_temp = tmp_nodes[i];
				tmp_nodes[i] = tmp_nodes[j];
				tmp_nodes[j] = node_temp;
			}

	/* Count the actual character type (the number of occurrences is not 0) */
	for(i = 0; i < NUM_MAX; ++i)
		if(tmp_nodes[i].weight == 0) 
			break;
	char_kinds = i;

	if (char_kinds == 1)
	{
		outfile = fopen(ofname, "wb");					/* Open the file that will be generated after compression */ 
		fwrite((char *)&char_kinds, sizeof(unsigned int), 1, outfile);		/* Write character type */
		fwrite((char *)&tmp_nodes[0].uch, sizeof(unsigned char), 1, outfile);		/* Write unique characters */
		fwrite((char *)&tmp_nodes[0].weight, sizeof(unsigned long), 1, outfile);		/* Write character frequency, which is the file length */
		free(tmp_nodes);
		fclose(outfile);
	}
	else
	{
		node_num = 2 * char_kinds - 1;		/* Calculate the number of nodes required to establish a Huffman tree based on the number of characters */
		huf_tree = (HufNode *)malloc(node_num*sizeof(HufNode));		/* Dynamically establish the required nodes of the Huffman tree */     

		/* Char_kinds nodes before initialization */ 
		for(i = 0; i < char_kinds; ++i) 
		{ 
			/* Copy the character and frequency of the temporary node to the tree node */ 
			huf_tree[i].uch = tmp_nodes[i].uch; 
			huf_tree[i].weight = tmp_nodes[i].weight;
			huf_tree[i].parent = 0; 
		}	
		free(tmp_nodes); /* Release the temporary storage area of character frequency statistics */

		/* Node_num-char_kins nodes after initialization */
		for(; i < node_num; ++i) 
			huf_tree[i].parent = 0; 

		CreateTree(huf_tree, char_kinds, node_num);		/* Creating a Huffman tree */

		HufCode(huf_tree, char_kinds);		/* Generating Huffman coding */

		/* Write characters and corresponding weights to reconstruct the Huffman tree when decompressing */ 
		outfile = fopen(ofname, "wb");					/* Open the file that will be generated after compression */
		fwrite((char *)&char_kinds, sizeof(unsigned int), 1, outfile);		/* Write character type */
		for(i = 0; i < char_kinds; ++i)
		{
			fwrite((char *)&huf_tree[i].uch, sizeof(unsigned char), 1, outfile);			/* Write characters (sorted, unchanged after reading) */ 
			fwrite((char *)&huf_tree[i].weight, sizeof(unsigned long), 1, outfile);		/* Write character corresponding weight */
		}

		/* File length and character encoding are written immediately after the character and weight information */
		fwrite((char *)&file_len, sizeof(unsigned long), 1, outfile);		/* Write file length */
		infile = fopen(ifname, "rb");		/* Open the file to be compressed in binary form */
		fread((char *)&char_temp, sizeof(unsigned char), 1, infile);     /* Read 8bits each time */
		while(!feof(infile))
		{
			/* Matching character corresponding code */
			for(i = 0; i < char_kinds; ++i)
				if(char_temp == huf_tree[i].uch)
					strcat(code_buf, huf_tree[i].code);

			/* 8 bits (one byte length) as the processing unit */
			while(strlen(code_buf) >= 8)
			{
				char_temp = '\0';		/* Clear the character temporary storage space and change to the temporary character corresponding code */
				for(i = 0; i < 8; ++i)
				{
					char_temp <<= 1;		/* Move one bit to the left to make room for the next bit */ 
					if(code_buf[i] == '1')
						char_temp |= 1;		/* When the encoding is "1", it is added to the lowest bit of the byte by the or operator. */
				}
				fwrite((char *)&char_temp, sizeof(unsigned char), 1, outfile);		/* Store the byte corresponding code in the file */
				strcpy(code_buf, code_buf+8);		/* Code Cache removes the first eight bits processed */ 
			}
			fread((char *)&char_temp, sizeof(unsigned char), 1, infile);     /* Read 8bits each time */
		}
		/* Processing the final less than 8bits encoding */ 
		code_len = strlen(code_buf);
		if(code_len > 0)
		{
			char_temp = '\0';		
			for(i = 0; i < code_len; ++i)
			{
				char_temp <<= 1;		
				if(code_buf[i] == '1')
					char_temp |= 1;
			}
			char_temp <<= 8-code_len;       /* Move the encoded field from the tail to the high byte */
			fwrite((char *)&char_temp, sizeof(unsigned char), 1, outfile);       /* Save the last byte */
		}

		/* Close file */
		fclose(infile);
		fclose(outfile);

		/* Free memory */
		for(i = 0; i < char_kinds; ++i)
			free(huf_tree[i].code);
		free(huf_tree);
	}
}

/*******************************************************************************
 * Description: Decompression function
 * inputs: data.Huffman
 * outputs: data.txt
 * Author: Haoran Huang
*******************************************************************************/

void extract(char *ifname, char *ofname)
{
	unsigned int i;
	unsigned long file_len;
	unsigned long writen_len = 0;		/* Control file write length */
	FILE *infile, *outfile;
	unsigned int char_kinds;		/* Storage character type */
	unsigned int node_num;
	HufTree huf_tree;
	unsigned char code_temp;		/* Temporary 8bits encoding */
	unsigned int root;		/* Save the root node index for matching encoding */

	infile = fopen(ifname, "rb");		/* Open compressed file in binary mode */
	/* Determine if the input file exists */ 
	if (infile == NULL)
		return;

	/* Read the characters and corresponding codes of the front end of the compressed file for reconstructing the Huffman tree */ 
	fread((char *)&char_kinds, sizeof(unsigned int), 1, infile);     /* Number of characters read */ 
	if (char_kinds == 1)
	{
		fread((char *)&code_temp, sizeof(unsigned char), 1, infile);     /* Read unique characters */
		fread((char *)&file_len, sizeof(unsigned long), 1, infile);     /* Read file length */ 
		outfile = fopen(ofname, "wb");					/* Open the file that will be generated after compression */
		while (file_len--)
			fwrite((char *)&code_temp, sizeof(unsigned char), 1, outfile);	
		fclose(infile);
		fclose(outfile);
	}
	else
	{
		node_num = 2 * char_kinds - 1;		/* Calculate the number of nodes required to establish a Huffman tree based on the number of characters */
		huf_tree = (HufNode *)malloc(node_num*sizeof(HufNode));		/* Dynamic allocation of Huffman tree node space */ 
		/* Read characters and corresponding weights and store them in the Huffman tree node */
		for(i = 0; i < char_kinds; ++i)     
		{
			fread((char *)&huf_tree[i].uch, sizeof(unsigned char), 1, infile);		/* Read character */ 
			fread((char *)&huf_tree[i].weight, sizeof(unsigned long), 1, infile);	/* Read characters corresponding weights */
			huf_tree[i].parent = 0;
		}
		/* Node_num-char_kins node's parent after initialization */
		for(; i < node_num; ++i) 
			huf_tree[i].parent = 0;

		CreateTree(huf_tree, char_kinds, node_num);		/* Reconstruct the Huffman tree (consistent with compression) */

		/* Read the character and weight information, then read the file length and encoding to decode */
		fread((char *)&file_len, sizeof(unsigned long), 1, infile);	/* Read file length */
		outfile = fopen(ofname, "wb");		/* Open the file that will be generated after compression */
		root = node_num-1;
		while(1)
		{
			fread((char *)&code_temp, sizeof(unsigned char), 1, infile);		/* Read the code of one character length */

			/* Handles the length of a character read (usually 8 bits) */ 
			for(i = 0; i < 8; ++i)
			{
				/* From the root down to the leaf node forward matching encoding corresponding characters */ 
				if(code_temp & 128)
					root = huf_tree[root].rchild;
				else
					root = huf_tree[root].lchild;

				if(root < char_kinds)
				{
					fwrite((char *)&huf_tree[root].uch, sizeof(unsigned char), 1, outfile);
					++writen_len;
					if (writen_len == file_len) break;		/* Control file length, jump out of inner loop */
					root = node_num-1;        /* Reset to root index, match next character */
				}
				code_temp <<= 1;		/* Shift the next bit of the code buffer to the highest bit for matching */ 
			}
			if (writen_len == file_len) break;		/* Control file length, jump out of the outer loop */
		}

		/* Close file */ 
		fclose(infile);
		fclose(outfile);

		/* Free memory */ 
		free(huf_tree);
	}
}

/****************************************************************************
* Initialises the system to a safe empty state.
* inputs: UTSCLISystemType main data structure for holding all system references
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void systemInit(UTSCLISystemType* system)
{
	/* Initialise all head nodes and int counts */
	system->headStaffNode = NULL; 
    system->headStudentNode = NULL;
    system->headSubjectNode = NULL;
    system->numStaff = 0;
    system->numStudents = 0;
    system->numSubjects = 0;
}

/****************************************************************************
* Deallocates memory used in the program.
* inputs: UTSCLISystemType main data structure for holding all system references
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void systemFree(UTSCLISystemType* system)
{
	StaffTypePtr currentStaffNode, nextStaffNode;
	StudentTypePtr currentStudentNode, nextStudentNode;
    SubjectTypePtr currentSubjectNode, nextSubjectNode;


	currentStaffNode = system->headStaffNode;

    /*Free staff nodes*/
	while (currentStaffNode != NULL)
	{
		nextStaffNode = currentStaffNode->nextStaffNode;
		freeStringLinkedList(&currentStaffNode->subjectsHeadNode);
		free(currentStaffNode);
		currentStaffNode = nextStaffNode;
	}

	system->numStaff = 0;
	system->headStaffNode = NULL;

    currentStudentNode = system->headStudentNode;
    /*Free student nodes*/
    while (currentStudentNode != NULL)
	{
		nextStudentNode = currentStudentNode->nextStudentNode;
		freeStringLinkedList(&currentStudentNode->announcementsHeadNode);
		free(currentStudentNode);
		currentStudentNode = nextStudentNode;
	}

	system->numStudents = 0;
	system->headStudentNode = NULL;

    currentSubjectNode = system->headSubjectNode;
    /*Free subject nodes*/
    while (currentSubjectNode != NULL)
	{
		nextSubjectNode = currentSubjectNode->nextSubjectNode;
		freeStringLinkedList(&currentSubjectNode->studentIDsEnrolledHead);
		free(currentSubjectNode);
		currentSubjectNode = nextSubjectNode;
	}

	system->numSubjects = 0;
	system->headSubjectNode = NULL;
}

/****************************************************************************
* This function deallocates memory used by string linked lists
* inputs: Pointer to a string linked list node
* outputs: StaffTypePtr a pointer to the created node
* Author: Adam Bursill
****************************************************************************/
void freeStringLinkedList(StringTypePtr* head)
{
	StringTypePtr currentStringNode, nextStringNode;

	currentStringNode = *head;

    /*Free string nodes*/
	while (currentStringNode != NULL)
	{
		nextStringNode = currentStringNode->nextStringNode;
		free(currentStringNode);
		currentStringNode = nextStringNode;
	}

	*head = NULL;
}

/****************************************************************************
* Creates a staff node and returns it
* inputs: String input of staff ID, name and password
* outputs: StaffTypePtr a pointer to the created node
* Author: Adam Bursill
****************************************************************************/
StaffTypePtr createStaffNode(const char* staffID, const char* staffName, const char* password)
{
	StaffTypePtr newStaff;

	/* Allocate memory for a new staff node*/
	if ((newStaff = malloc(sizeof(StaffType))) == NULL)
	{
		fprintf(stderr,"\nError - Memory allocation failed\n");
		fprintf(stderr,"Aborting program");
		exit(EXIT_FAILURE);
	}

	strcpy(newStaff->staffID, staffID); /* Copy strings from newStaff to respective locations */
	strcpy(newStaff->staffName, staffName);
	strcpy(newStaff->password, password);

	newStaff->nextStaffNode = NULL; /* Terminate the node by setting next pointer to NULL */
	newStaff->subjectsHeadNode = NULL;
	newStaff->numOfSubjectsManaged = 0;

	return newStaff;

}

/****************************************************************************
* Creates a student node and returns it
* inputs: String input of student ID, name and their password
* outputs: StudentTypePtr a pointer to the created node
* Author: Adam Bursill
****************************************************************************/
StudentTypePtr createStudentNode(const char* studentID, const char* studentName, const char* password)
{
	StudentTypePtr newStudent;

	/* Allocate memory for a new student node*/
	if ((newStudent = malloc(sizeof(StudentType))) == NULL) /* Error check allocation */
	{
		fprintf(stderr,"\nError - Memory allocation failed\n");
		fprintf(stderr,"Aborting program");
		exit(EXIT_FAILURE);
	}

	strcpy(newStudent->studentID, studentID); /* Copy strings from newStaff to respective locations */
	strcpy(newStudent->studentName, studentName);
	strcpy(newStudent->password, password);

	newStudent->numOfAnnoucements = 0; /* Initialise new student node attributes */
	newStudent->numOfAssignments = 0;
	newStudent->numOfEnrolledClasses = 0;

	newStudent->announcementsHeadNode = NULL; /* Terminate the new student node by setting next pointer to NULL */

	newStudent->nextStudentNode = NULL;

	return newStudent;
}

/****************************************************************************
* Creates a subject node and returns it
* inputs: String input to subject ID, string input to subject name
* outputs: StudentTypePtr a pointer to the created node
* Author: Adam Bursill
****************************************************************************/
SubjectTypePtr createSubjectNode(const char* subjectID, const char* subjectName)
{
	SubjectTypePtr newSubject;
	/* Allocate memory for a new subject node*/
	if ((newSubject = malloc(sizeof(SubjectType))) == NULL) /* Error check allocation */
	{
		fprintf(stderr,"\nError - Memory allocation failed\n");
		fprintf(stderr,"Aborting program");
		exit(EXIT_FAILURE);
	}

	strcpy(newSubject->subjectID, subjectID);
	strcpy(newSubject->subjectName, subjectName);

	newSubject->studentIDsEnrolledHead = NULL; /* Terminate the new subject node by setting next pointer to NULL */
	newSubject->numOfEnrolledStudents = 0; /* Initialise students counter */
	newSubject->nextSubjectNode = NULL;

	return newSubject;
}

/****************************************************************************
* Creates a string node and returns it
* inputs: A string input for data allocation to new string node
* outputs: StudentTypePtr a pointer to the created node
* Author: Adam Bursill
****************************************************************************/
StringTypePtr createStringNode(const char* string)
{
	StringTypePtr newString;
	/* Allocate memory for a new string node*/
	if ((newString = malloc(sizeof(StringType))) == NULL) /* Error check allocation */
	{
		fprintf(stderr,"\nError - Memory allocation failed\n");
		fprintf(stderr,"Aborting program");
		exit(EXIT_FAILURE);
	}

	strcpy(newString->string, string); /* Copy string content to respective location in memory */


	newString->nextStringNode = NULL; /* Terminate node by setting next pointer to NULL */

	return newString;
}

/****************************************************************************
* Adds a staff node to the linked list
* inputs: Pointer to the current system state, pointer to a staff node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void addStaffNodeToList(UTSCLISystemType* system, StaffTypePtr node)
{
	StaffTypePtr staffCurrent, previous;

	staffCurrent = system->headStaffNode;
	previous = NULL;

    /*Move pointers to the end of the linked list*/
	while (staffCurrent != NULL)
	{
		previous = staffCurrent;
		staffCurrent = staffCurrent->nextStaffNode;
	}

    /*If there were no nodes in the list*/
	if (previous == NULL)
	{
		system->headStaffNode = node;
	}
	else
	{	/* Otherwise add node to end of the list*/	
		previous->nextStaffNode = node;
	}
    system->numStaff++;

}

/****************************************************************************
* Adds a student node to the linked list
* inputs: Pointer to the current system state, pointer to a student node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void addStudentNodeToList(UTSCLISystemType* system, StudentTypePtr node)
{
	StudentTypePtr staffCurrent, previous;

	staffCurrent = system->headStudentNode;
	previous = NULL;

    /*Move pointers to the end of the linked list*/
	while (staffCurrent != NULL)
	{
		previous = staffCurrent;
		staffCurrent = staffCurrent->nextStudentNode;
	}

    /*If there were no nodes in the list*/
	if (previous == NULL)
	{
		system->headStudentNode = node;
	}
	else
	{		
		/* Otherwise add node to end of the list*/	
		previous->nextStudentNode = node;
	}
    system->numStudents++;

}

/****************************************************************************
* Adds a subjects node to the linked list
* inputs: Pointer to the current system state, pointer to a subject node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void addSubjectNodeToList(UTSCLISystemType* system, SubjectTypePtr node)
{
	SubjectTypePtr staffCurrent, previous;

	staffCurrent = system->headSubjectNode;
	previous = NULL;

    /*Move pointers to the end of the linked list*/
	while (staffCurrent != NULL)
	{
		previous = staffCurrent;
		staffCurrent = staffCurrent->nextSubjectNode;
	}

    /*If there were no nodes in the list*/
	if (previous == NULL)
	{
		system->headSubjectNode = node;
	}
	else
	{		
		/* Otherwise add node to end of the list*/	
		previous->nextSubjectNode = node;
	}
    system->numSubjects++;

}

/****************************************************************************
* Adds a string node to the linked list
* inputs: Pointer to a string head node, pointer to a string node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void addStringNodeToList(StringTypePtr* head, StringTypePtr node)
{
	StringTypePtr staffCurrent, previous;

	staffCurrent = *head;
	previous = NULL;

    /*Move pointers to the end of the linked list*/
	while (staffCurrent != NULL)
	{
		previous = staffCurrent;
		staffCurrent = staffCurrent->nextStringNode;
	}

    /*If there were no nodes in the list*/
	if (previous == NULL)
	{
		*head = node;

		#ifdef DEBUG
			printf("DEBUG - Setting new string node to head node. \n\n");
		#endif
	}
	else
	{		
		previous->nextStringNode = node;

		#ifdef DEBUG
			printf("DEBUG - Seting new string node to previous next node. \n\n");
		#endif
	}
}

/****************************************************************************
* This function is used for DEBUGing and to print out the staff linked list
* inputs: the head of the staff linked list node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void printStaffLinkedList(StaffTypePtr headNode)
{
	StaffTypePtr staffCurrent;

	staffCurrent = headNode;

	while (staffCurrent != NULL)
	{
		printf("Staff ID: %s\n", staffCurrent->staffID);
		printf("Staff name: %s\n", staffCurrent->staffName);
		printf("Staff password: %s\n", staffCurrent->password);
		printf("Staff total subjects: %d\n", staffCurrent->numOfSubjectsManaged);
		printf("Subjects: ");
		printStringLinkedList(staffCurrent->subjectsHeadNode, FALSE, FALSE);
		staffCurrent = staffCurrent->nextStaffNode;
		printf("\n");
	}
}

/****************************************************************************
* This function is used for DEBUGing and to print out the student linked list
* inputs: The head of the student linked list node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void printStudentLinkedList(StudentTypePtr headNode)
{
	StudentTypePtr studentCurrent;
	int i;

	studentCurrent = headNode;

	while (studentCurrent != NULL)
	{
		printf("Student ID: %s\n", studentCurrent->studentID);
		printf("Student name: %s\n", studentCurrent->studentName);
		printf("Student password: %s\n", studentCurrent->password);
		printf("Number of enrolled Subjects: %d\n", studentCurrent->numOfEnrolledClasses);
		printf("Enrolled Subject ID's: ");

		/* For every enrolled class string in the enroled class array print it out*/
		for(i = 0; i < studentCurrent->numOfEnrolledClasses; i++)
		{
			printf("%s, ", studentCurrent->enroledClasses[i]);
		}

		printf("\nNumber of assignments submitted: %d\n", studentCurrent->numOfAssignments);

		 /*print all details of assignemnts in the assignments array*/
		for(i = 0; i < studentCurrent->numOfAssignments; i++)
		{
			printf("\n");
			printf("Assignment Subject ID: %s\n", studentCurrent->assignments[i].subject);
			printf("Assignment name: %s\n", studentCurrent->assignments[i].assignmentName);
			printf("Filename: %s\n", studentCurrent->assignments[i].filename);
			printf("Mark: %f", studentCurrent->assignments[i].mark);
		}
		printf("\n");
		printf("\n");

		printf("Student amount of announcements: %d\n", studentCurrent->numOfAnnoucements);
		printf("Announcements: \n\n");
		printStringLinkedList(studentCurrent->announcementsHeadNode, TRUE, FALSE);

		studentCurrent = studentCurrent->nextStudentNode;
		printf("\n");
	}
}

/****************************************************************************
* This function is used for DEBUGing and to print out the student linked list
* inputs: The head of the subject linked list node
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void printSubjectLinkedList(SubjectTypePtr headNode)
{
	SubjectTypePtr staffCurrent;

	staffCurrent = headNode;

	while (staffCurrent != NULL)
	{
		printf("Subject ID: %s\n", staffCurrent->subjectID);
		printf("Subject name: %s\n", staffCurrent->subjectName);
		printf("Amount of Students in Subject: %d\n", staffCurrent->numOfEnrolledStudents);
		printf("Enrolled Student IDs: ");
		printStringLinkedList(staffCurrent->studentIDsEnrolledHead, FALSE, FALSE);

		staffCurrent = staffCurrent->nextSubjectNode;
		printf("\n");
	}
}

/****************************************************************************
* This function is used for to print out the avalable subject for student to 
* see what they can enrol in.
* inputs: Pointer to the head subject node in subject linked list
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void printSubjectsForStudentToEnrol(SubjectTypePtr headNode)
{
	SubjectTypePtr staffCurrent;
	int count = 1;

	staffCurrent = headNode;

	while (staffCurrent != NULL)
	{
		printf("%d. ", count);
		printf("Subject ID: %s | ", staffCurrent->subjectID);
		printf("Subject name: %s\n", staffCurrent->subjectName);

		staffCurrent = staffCurrent->nextSubjectNode;
		count++;
	}
}

/****************************************************************************
* This function is used for debugging and to print out any string linked list
* inputs: Head string linked list node, BOOLEAN if each string should be 
* on a new line, BOOLEAN is each string should be numbered
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void printStringLinkedList(StringTypePtr headNode, BOOLEAN eachOnNewLine, 
	BOOLEAN numberStrings)
{
	StringTypePtr staffCurrent;
	staffCurrent = headNode;
	int counter = 1;

	while (staffCurrent != NULL)
	{
		/* If true add a counting number to the start of each print out */
		if(numberStrings)
		{
			printf("%d. ", counter);
		}
		printf("%s, ", staffCurrent->string);
		/* If true add a newline at the end of each loops print out*/
		if(eachOnNewLine)
		{
			printf("\n");
		}

		staffCurrent = staffCurrent->nextStringNode;
		counter++;
	}
	printf("\n");
}

/****************************************************************************
* This function is returns a string node at the given nodeNumber
* inputs: Pointer to a string head node, index number of string node to return
* outputs: string node at index of the input int
* Author: Adam Bursill
****************************************************************************/
StringTypePtr getStringLinkedListNode(StringTypePtr headNode, int nodeNumber)
{
	StringTypePtr staffCurrent;
	staffCurrent = headNode;
	int counter = 1;

	/* Count through each loop until we get to the one we are after, unless return NULL*/
	while (staffCurrent != NULL)
	{
		if(counter == nodeNumber)
		{
			return staffCurrent;
		}

		staffCurrent = staffCurrent->nextStringNode;
		counter++;
	}

	return NULL;
}

/****************************************************************************
* This function is returns a subject node at the given nodeNumber
* inputs: Pointer to a subject head node, index number of subject node to return
* outputs: subject node at index of the input int
* outputs: subject node at index of int passing in
* Author: Adam Bursill
****************************************************************************/
SubjectTypePtr getSubjectLinkedListNode(SubjectTypePtr headNode, int nodeNumber)
{
	SubjectTypePtr subjectCurrent;
	subjectCurrent = headNode;
	int counter = 1;

	while (subjectCurrent != NULL)
	{
		if(counter == nodeNumber)
		{
			return subjectCurrent;
		}

		subjectCurrent = subjectCurrent->nextSubjectNode;
		counter++;
	}

	return NULL;
}



/****************************************************************************
* This function is used for buffer clearing
* inputs: None
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF);

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* This function is used for checking input for errors such as new line, or 
 input larger then storage amount, or is the input is a digit
* inputs: the max length of the string, the string, the promt to the user, 
* should it be a digit or not
* outputs: int return to menu const to each if the calling function should return
* Author: Adam Bursill
****************************************************************************/
int  validateInput(int strLen, char* userIn, const char* prompt, BOOLEAN isDigit)
{
	int result = FALSE;

	do
	{
		printf("%s", prompt);

		fgets(userIn, strLen + EXTRA_SPACES, stdin); /*If char is new line */
		if (userIn[0] == '\n')
		{
			printf("Operation cancelled\n\n");
			#ifdef DEBUG
				printf("DEBUG - New Line As first char");
			#endif
			
			return RETURN_TO_MENU; /* Cancel the operation */
		}

		if (userIn[strlen(userIn) - 1] != '\n')
		{
			printf("Error - Input was to long\n\n");
			readRestOfLine();
			result = FALSE;
			continue; /*There is an error so skip the rest of the loop*/
		}
		else
		{
			userIn[strlen(userIn) - 1] = '\0';
			result = TRUE;
		}

		if(isDigit)
		{
			int i;
			for (i = 0; i < strlen(userIn); i++) /* For each char, check if it is a number */
			{
				if(userIn[i] - '0' < 0 || userIn[i] - '0' > 9)
				{
					printf("Error - Input not a valid number\n\n");
					result = FALSE;
					#ifdef DEBUG
						printf("DEBUG - Input not a digit\n");
					#endif
					break;
				}
			}
		}		
	}
	while(result == FALSE);

	return result;
}

/****************************************************************************
* This function is used for searching and returning a student node in the linked
* list that matches the passed in studentID
* inputs: Head of the student linked list, the student ID to search for
* outputs: the found Student node or null if nothing found
* Author: Adam Bursill
****************************************************************************/
StudentTypePtr searchForStudentAndReturn(StudentTypePtr headNode, const char* studentID)
{
	StudentTypePtr staffCurrent;

	staffCurrent = headNode;

	while (staffCurrent != NULL) /* Traverse through linked list and return current student node */
	{
		if(strcmp(staffCurrent->studentID, studentID) == 0)
		{
			return staffCurrent;
		}
		staffCurrent = staffCurrent->nextStudentNode;
	}

	return NULL;
}

/****************************************************************************
* This function is used for searching and returning a staff node in the linked
* list that matches the passed in staffID
* inputs: Head of the staff linked list, the staff ID to search for
* outputs: the found staff node or null if nothing found
* Author: Adam Bursill
****************************************************************************/
StaffTypePtr searchForStaffAndReturn(StaffTypePtr headNode, const char* staffID)
{
	StaffTypePtr staffCurrent;

	staffCurrent = headNode;

	while (staffCurrent != NULL) /* Traverse through linked list and return current staff node */
	{
		if(strcmp(staffCurrent->staffID, staffID) == 0)
		{
			return staffCurrent;
		}
		staffCurrent = staffCurrent->nextStaffNode;
	}

	return NULL;
}

/****************************************************************************
* This function is used for searching and returning a subject node in the linked
* list that matches the passed in subjectID
* inputs: Head of the subject linked list, the subject ID to search for
* outputs: the found subject node or null if nothing found
* Author: Adam Bursill
****************************************************************************/
SubjectTypePtr searchForSubjectAndReturn(SubjectTypePtr headNode, const char* subjectID)
{
	SubjectTypePtr staffCurrent;

	staffCurrent = headNode;

	while (staffCurrent != NULL) /* Traverse through linked list and return current subject node */
	{
		if(strcmp(staffCurrent->subjectID, subjectID) == 0)
		{
			return staffCurrent;
		}
		staffCurrent = staffCurrent->nextSubjectNode;
	}

	return NULL;
}

/****************************************************************************
* This function is used for searching and printing out student details of any
* students currently enrolled in a subject. If has the option on only printing 
* out student that have submitted an assignment for that subject ID.
* inputs: Pointer to the main system reference, the subject ID to search against,
* BOOLEAN to set if it should only print student with submitted assigment or not
* outputs: int if it fails because there are no student in that subject
* Author: Adam Bursill
****************************************************************************/
int printStudentDetailsInSubject(UTSCLISystemType* system, const char* subjectID, BOOLEAN onlyShowAssignmentSubmited)
{
	StringTypePtr staffCurrent; /* Initialise instances of string, student and assignmment pointers */
	StudentTypePtr studentInSubject;
	int i;
	AssignmentType* assignment;
	BOOLEAN assignmentSubmitted;

	SubjectTypePtr subject = searchForSubjectAndReturn(system->headSubjectNode, subjectID);

	if(subject->numOfEnrolledStudents == 0) /* Check enrollment count */
    {
        printf("\nError - No students currently enrolled\n\n");
        return 1;
    }
	
	staffCurrent = subject->studentIDsEnrolledHead;

	while (staffCurrent != NULL) /* Traverse through the staff linked list */
	{
		assignment = NULL;
		assignmentSubmitted = FALSE;
		studentInSubject = searchForStudentAndReturn(system->headStudentNode, staffCurrent->string);

		for(i = 0; i < studentInSubject->numOfAssignments; i++) /* Traverse through the assignments */
		{
			if(strcmp(studentInSubject->assignments[i].subject, subjectID) == 0) /* If submitted, check TRUE */
			{
				assignment = &studentInSubject->assignments[i];
				assignmentSubmitted = TRUE;
			}
		}		

		/* Only show student details that have submitted an assigment for the subject*/
		if(assignmentSubmitted == FALSE && onlyShowAssignmentSubmited)
		{
			continue; /* If assignment not submitted, skip the rest of the loop */
		}

		printf("Student ID: %s | Student Name: %s | ", studentInSubject->studentID, studentInSubject->studentName);
		
		if(assignmentSubmitted)
		{
			/* If submitted, print current marks */
			printf("Assignment Submitted = True | Current Mark = %f", assignment->mark);
		}
		else
		{
			/* Print assignment status FALSE if not submitted */
			printf("Assignment Submitted = False");
		}

		printf("\n");

		staffCurrent = staffCurrent->nextStringNode; /* Set current staff to next to traverse through linked list */	
	}

	return 0;
}

/****************************************************************************
* This function searches through all students currently enroled in the subject
* and adds an accouement string node to the students accounment string list
* inputs: Pointer to the main system reference, pointer to the subject to search
* against, the accountment to add to the students
* outputs: None
* Author: Adam Bursill
****************************************************************************/
void addAnnoucementsToStudentsInSubject(UTSCLISystemType* system, 
SubjectTypePtr subject, char* annoucement)
{
	StringTypePtr studentIDStringCurrent;
	studentIDStringCurrent = subject->studentIDsEnrolledHead;

	while (studentIDStringCurrent != NULL) /* Traverse through string pointer */
	{
		/* Get the student node that matchs the student ID enroled in this subject*/
		StudentTypePtr student = searchForStudentAndReturn(system->headStudentNode, 
			studentIDStringCurrent->string);

		StringTypePtr newAnnoucementNode = createStringNode(annoucement);

		/* Add the string node to the list of strings for subject */
		addStringNodeToList(&student->announcementsHeadNode, newAnnoucementNode); 
		student->numOfAnnoucements++; /* Increment announcements counter */

		/* Set current node to next to traverse */
		studentIDStringCurrent = studentIDStringCurrent->nextStringNode; 
	}
}
