/*******************************************************************************
 * 48430 Fundamentals of C Programming - Assignment 2
 * 
 * Group 28
 * Header file for main utility file
*******************************************************************************/


#ifndef UTS_UTILITY_H
#define UTS_UTILITY_H

/* Data file names*/
#define STAFF_DB "staff_db"
#define STUDENT_DB "students_db"
#define SUBJECT_DB "subject_db"

/* Used for reading file input*/
#define DELIM "|"
#define DELIM2 "."
#define FILE_EXT ".txt"
#define ENCRYPTION_KEY 3

/* Used for compression*/
#define NUM_MAX 256
#define ULONG_MAX pow(2,32)

/* Used for reading file input*/
#define MAX_FILE_INPUT_LENGTH 2048

/* Temporary node for counting character frequencies */
typedef struct {
	unsigned char uch;			/* Unsigned characters in units of 8bits */ 
	unsigned long weight;		/* Frequency of occurrence of each class (differentiated by binary code) */ 
} TmpNode;

/* Huffman Tree Node */ 
typedef struct {
	unsigned char uch;				/* Unsigned characters in units of 8bits */ 
	unsigned long weight;			/* Frequency of occurrence of each class (differentiated by binary code) */ 
	char *code;						/* Huffman coding for characters (dynamic allocation of storage space) */ 
	int parent, lchild, rchild;		/* Define parents and left and right children */ 
} HufNode, *HufTree;


/* Function prototypes. */
void load_system_state(UTSCLISystemType* system);
void save_system_state(UTSCLISystemType* system);
void save_string_linked_list_to_file(FILE* filePtr, StringTypePtr headNode);
int decrypt_file(const char * file_name, int key);
int encrypt_file(const char * file_name, int key);
void encrypt_password(const char* password, char* encryptedPassword);
void select(HufNode *huf_tree, unsigned int n, int *s1, int *s2);
void CreateTree(HufNode *huf_tree, unsigned int char_kinds, unsigned int node_num);
void HufCode(HufNode *huf_tree, unsigned char_kinds);
void compress(char *ifname, char *ofname);
void extract(char *ifname, char *ofname);
void systemInit(UTSCLISystemType* system);
void systemFree(UTSCLISystemType* system);
void freeStringLinkedList(StringTypePtr* head);
StaffTypePtr createStaffNode(const char* staffID, const char* staffName, const char* password);
StudentTypePtr createStudentNode(const char* studentID, const char* studentName, const char* password);
SubjectTypePtr createSubjectNode(const char* subjectID, const char* subjectName);
StringTypePtr createStringNode(const char* string);
void addStaffNodeToList(UTSCLISystemType* system, StaffTypePtr node);
void addStudentNodeToList(UTSCLISystemType* system, StudentTypePtr node);
void addSubjectNodeToList(UTSCLISystemType* system, SubjectTypePtr node);
void addStringNodeToList(StringTypePtr* head, StringTypePtr node);
void readRestOfLine();
int  validateInput(int strLen, char* userIn, const char* prompt, BOOLEAN isDigit);
void printStaffLinkedList(StaffTypePtr headNode);
void printStudentLinkedList(StudentTypePtr headNode);
void printSubjectLinkedList(SubjectTypePtr headNode);
void printSubjectsForStudentToEnrol(SubjectTypePtr headNode);
void printStringLinkedList(StringTypePtr headNode, BOOLEAN eachOnNewLine, BOOLEAN numberStrings);
StudentTypePtr searchForStudentAndReturn(StudentTypePtr headNode, const char* studentID);
StaffTypePtr searchForStaffAndReturn(StaffTypePtr headNode, const char* staffID);
SubjectTypePtr searchForSubjectAndReturn(SubjectTypePtr headNode, const char* subjectID);
StringTypePtr getStringLinkedListNode(StringTypePtr headNode, int nodeNumber);
SubjectTypePtr getSubjectLinkedListNode(SubjectTypePtr headNode, int nodeNumber);
int printStudentDetailsInSubject(UTSCLISystemType* system, const char* subjectID, BOOLEAN onlyShowAssignmentSubmited);
void addAnnoucementsToStudentsInSubject(UTSCLISystemType* system, SubjectTypePtr subject, char* annoucement);


#endif
