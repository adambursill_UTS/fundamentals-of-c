#****************************************************************************
# 48430 Fundamentals of C Programming - Assignment 2
# Group 28
# Arthur        : AdamBursill
#***************************************************************************/

#Complile flags
CFLAGS = -ansi -Wall -Werror -lm
#Complier
CC = gcc


UTS_CLI: UTS_CLI.o UTS_staff_options.o UTS_student_options.o UTS_system_options.o UTS_utility.o
	$(CC) -o UTS_CLI UTS_CLI.o UTS_staff_options.o UTS_student_options.o UTS_system_options.o UTS_utility.o
	
UTS_utility.o: UTS_utility.c UTS_utility.h
	$(CC) $(CFLAGS) -c UTS_utility.c

UTS_system_options.o: UTS_system_options.c UTS_system_options.h
	$(CC) $(CFLAGS) -c UTS_system_options.c

UTS_student_options.o: UTS_student_options.c UTS_student_options.h
	$(CC) $(CFLAGS) -c UTS_student_options.c

UTS_staff_options.o: UTS_staff_options.c UTS_staff_options.h
	$(CC) $(CFLAGS) -c UTS_staff_options.c
	
UTS_CLI.o: UTS_CLI.c UTS_CLI.h
	$(CC) $(CFLAGS) -c UTS_CLI.c

clean: 
	rm -f *.o core UTS_CLI